Updating/adding blends websites
===============================

Updating a blend
----------------

The www/ folder contains the static websites as they show up on the Debian
mirrors. Apply your changes there, commit and push.


Adding a new blend
------------------

You could just create it in the www/ folder.
If instead you want to use the build framework, you have to make your changes
the www-src/ folder.

Each blend sub-site has a layout in the www-src/_layouts directory. You can copy
the template.html file there to <blend-name>.html. Each blend also has an
independent navigation menu in www-src/_includes. You can copy the template_menu.html
file to <blend-name>_menu.html. Inside the www-src/template directory you can see
an example home page you can use.

In order to build your changes you will need to install the jekyll package:

   apt-get install jekyll

After you have made your changes, cd into the www-src/ folder and run:

   jekyll build

This will create the a folder containing the new static blend website in the
www-sandbox/ folder; you can use a browser to check the site, also images
added to the img/folder/ will show up correctly in this case.

Please note: the www-sandbox/ folder is deleted each time you run 'jekyll build'
inside the www-src/ folder.

Copy your new site folder from the www-sandbox/ folder to the www/ one,
remove the www-sandbox/ folder, commit and push.
