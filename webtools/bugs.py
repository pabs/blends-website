#!/usr/bin/python3
# Copyright 2013: Andreas Tille <tille@debian.org>
# License: GPL

import codecs
from sys import argv, stderr, exit
import os
import psycopg2
import json
import re
import time
from datetime import datetime
from email.utils import formatdate
import email.utils
import gettext

from genshi.template import TemplateLoader
from genshi import Markup

from blendstasktools import ReadConfig, RowDictionaries, CheckOrCreateOutputDir, SetFilePermissions

# PORT = 5441
UDDPORT = 5452
PORT = UDDPORT
DEFAULTPORT = 5432

SLOWQUERYREPORTLIMIT = 30

debug = 0

###########################################################################################
# Define several prepared statements to query UDD
connection_pars = [
    {'host': 'localhost', 'port': PORT, 'user': 'guest', 'database': 'udd'},
    {'service': 'udd'},
    {'host': 'localhost', 'port': DEFAULTPORT, 'user': 'guest', 'database': 'udd'},

    # Hmmm, I observed a really strange behaviour on one of my machines where
    # connecting to localhost does not work but 127.0.0.1 works fine.  No idea
    # why ... but this should do the trick for the moment
    {'host': '127.0.0.1', 'port': DEFAULTPORT, 'user': 'guest', 'database': 'udd'},

    # Public UDD mirror as last resort
    {'host': 'public-udd-mirror.xvm.mit.edu', 'port': 5432,
     'user': 'public-udd-mirror', 'password': 'public-udd-mirror', 'database': 'udd'},
]
conn = None
for par in connection_pars:
    try:
        conn = psycopg2.connect(**par)
        break
    except psycopg2.OperationalError as err:
        continue

if conn is None:
    raise err

conn.set_client_encoding('utf-8')
curs = conn.cursor()
# uddlog = open('logs/uddquery.log', 'w')

def _execute_udd_query(query):
    try:
        t = time.time()
        curs.execute(query)
        elapsed_time = time.time() - t
        if elapsed_time > SLOWQUERYREPORTLIMIT:  # report what query took longer than SLOWQUERYREPORTLIMIT seconds
            print("Time: %s\nQuery: %s" % (str(elapsed_time), query))
    except psycopg2.ProgrammingError as err:
        stderr.write("Problem with query\n%s\n%s\n" % (query, str(err)))
        exit(-1)
    except psycopg2.DataError as err:
        stderr.write("%s; query was\n%s\n" % (err, query))


def main():

    if len(argv) <= 1:
        stderr.write("Usage: %s <Blend name>\n" % argv[0])
        exit(-1)

    blendname = argv[1]
    config = ReadConfig(blendname)

    # Metadata of packages that might have bugs
    query = """PREPARE query_bug_packages (text) AS
      SELECT distinct sources.source, task, CASE WHEN (tasks.dependency = 'd' OR tasks.dependency = 'r') AND component = 'main' AND experimental_flag > 0 THEN 'depends' ELSE 'suggests' END AS status,
                      homepage, CASE WHEN vcs_browser IS NULL THEN '#' ELSE vcs_browser END AS vcs_browser, maintainer
        FROM (
          SELECT s.source, b.component, s.homepage, s.vcs_browser, s.maintainer, s.version, row_number() OVER (PARTITION BY s.source ORDER BY s.version DESC)
            FROM blends_dependencies b
            JOIN packages p ON p.package = b.package
            JOIN bugs bu    ON bu.source = p.source
            JOIN sources s  ON s.source  = p.source
            WHERE blend = $1 AND b.distribution = 'debian'
        ) sources
        -- check status of dependency relation because only suggested packages are less important for bugs sentinel
        LEFT OUTER JOIN (
          SELECT source, task, dependency FROM (
            SELECT p.source, b.task, bdp.dependency, row_number() OVER (PARTITION BY p.source, b.task ORDER BY bdp.priority)
              FROM packages p
              JOIN blends_dependencies b ON b.package = p.package
              JOIN sources s ON p.source = s.source AND p.release = s.release
              JOIN blends_dependencies_priorities bdp ON b.dependency = bdp.dependency
              WHERE b.blend = $1
          ) tmp
          WHERE row_number = 1
        ) tasks ON sources.source = tasks.source
        -- Check, whether a package is in experimental only which makes it less important for bugs sentinel
        LEFT OUTER JOIN (
          SELECT source, MAX(sort) AS experimental_flag FROM sources s
            JOIN releases r ON s.release = r.release
          GROUP BY source
        ) exp ON sources.source = exp.source
        WHERE row_number = 1
        ORDER BY source;
    """
    _execute_udd_query(query)

    # Actual bugs in packages of Blends dependencies
    query = """PREPARE query_bugs (text) AS
      SELECT source, bu.id, title, severity, status, done_by, tags FROM (
        SELECT distinct bu.source, bu.id, bu.title, bu.severity, bu.status, bu.done AS done_by
          FROM blends_dependencies b
          JOIN packages p ON p.package = b.package
          JOIN bugs bu    ON bu.source = p.source
          WHERE blend = $1 AND b.distribution = 'debian'
        ) bu
        LEFT OUTER JOIN (
          SELECT id, array_agg(tag) AS tags FROM bugs_tags GROUP BY id
        ) bt ON bu.id = bt.id
        ORDER BY source, bu.id;
    """
    _execute_udd_query(query)

    # What tasks are involved
    query = """PREPARE query_get_tasks (text) AS
      SELECT task, title, description, long_description FROM blends_tasks WHERE blend = $1 ORDER BY task;
    """
    _execute_udd_query(query)

    STATES = ['depends', 'suggests', 'done']
    SEVERITIES = ('critical', 'grave', 'serious', 'important', 'normal', 'minor', 'wishlist')

    # Sense of weight: We want to find a measure how much care a metapackage needs.
    # So we build the weighted sums of bugs and define limits for the status
    # The weights below are used for suggested packages.  In case a package has
    # a stronger dependency (Depends, Recommends) the weight is multiplied by 3
    WEIGHT = {
        'critical'  : 10,
        'grave'     : 10,
        'serious'   : 10,
        'important' : 5,
        'normal'    : 3,
        'minor'     : 1,
        'wishlist'  : 0
    }
    BAD          = 100  # if weighted bug sum >= BAD, the meta package is in a bad shape
                        # Dependent packages might have 3 or more 5 RC bugs
    PASS         = 70  # this deserves a look - potentially two RC bugs in dependent packages
    SATISFACTORY = 50  # consider looking at this
    GOOD         = 30  # at least no RC bug in a dependent package
    VERYGOOD     = 10  # nothing burning
    EXCELLENT    = 5   # There is no real need to look at this meta package

    # initialise bugs_data dictionary for all tasks
    _execute_udd_query("EXECUTE query_get_tasks('%s')" % blendname)
    bugs_data = {}
    if curs.rowcount > 0:
        for t in RowDictionaries(curs):
            task = t['task']
            bugs_data[task] = {}
            bugs_data[task]['title']            = t['title']
            bugs_data[task]['description']      = t['description']
            bugs_data[task]['long_description'] = t['long_description']
            bugs_data[task]['nopenbugs']        = 0
            bugs_data[task]['ndonebugs']        = 0
            bugs_data[task]['weighttask']       = 0
            for status in STATES:
                bugs_data[task][status + '_l']     = []  # enable sorting
                bugs_data[task][status]            = {}
                bugs_data[task][status]['sources'] = []
                if status != 'done':
                    bugs_data[task][status]['severitysummary'] = ''  # string listing number of bugs in different severity / dependency classes
                    bugs_data[task][status]['severities'] = {}
                    for s in SEVERITIES:
                        bugs_data[task][status]['severities'][s] = 0
    else:
        stderr.write("No tasks metadata received for Blend %s\n" % blendname)
        exit(1)

    # Fetch bugs of all Blends dependencies and store them in a dictionary
    _execute_udd_query("EXECUTE query_bugs('%s')" % blendname)
    bugs = {}
    if curs.rowcount > 0:
        for bug in RowDictionaries(curs):
            if bug['source'] not in bugs:
                bugs[bug['source']] = {}
                bugs[bug['source']]['severities'] = {}
                for s in SEVERITIES:
                    bugs[bug['source']]['severities'][s] = 0
                bugs[bug['source']]['nopenbugs'] = 0
                bugs[bug['source']]['ndonebugs'] = 0
                bugs[bug['source']]['open'] = []
                bugs[bug['source']]['done'] = []
            b = {}
            for k in list(bug.keys()):
                if k in ('source', 'status') :
                    continue
                if k == 'title':
                    b[k] = bug[k]
                elif k == 'tags':
                    komma = ''
                    b['tags'] = ''
                    if bug[k]:
                        for tag in bug[k]:
                            b['tags'] += komma + tag
                            komma      = ', '
                else:
                    b[k] = bug[k]
            if bug['status'] == 'done':
                bugs[bug['source']]['done'].append(b)
                bugs[bug['source']]['ndonebugs'] += 1
            else:
                bugs[bug['source']]['open'].append(b)
                bugs[bug['source']]['nopenbugs'] += 1
                bugs[bug['source']]['severities'][bug['severity']] += 1
    else:
        stderr.write("No bug data received for Blend %s\n" % blendname)
        exit(1)

    # Merge metadata of packages and bugs together in bugs_data dictionary, also do statistics about bugs
    _execute_udd_query("EXECUTE query_bug_packages('%s')" % blendname)
    if curs.rowcount > 0:
        for pkg in RowDictionaries(curs):
            task = pkg['task']
            sources = {}
            sources['source']      = pkg['source']
            sources['homepage']    = pkg['homepage']
            sources['vcs_browser'] = pkg['vcs_browser']
            (_name, _url) = email.utils.parseaddr(pkg['maintainer'])
            sources['maintainer'] = {'name': _name, 'email': _url}
            if pkg['status'] == 'depends':
                sources['bugs'] = bugs[pkg['source']]['open']
                bugs_data[task][pkg['status'] + '_l'].append(pkg['source'])
                for s in SEVERITIES:
                    bugs_data[task][pkg['status']]['severities'][s] += bugs[pkg['source']]['severities'][s]
                    bugs_data[task]['weighttask'] += 3 * WEIGHT[s] * bugs[pkg['source']]['severities'][s]
            elif pkg['status'] == 'suggests':
                sources['bugs'] = bugs[pkg['source']]['open']
                bugs_data[task][pkg['status'] + '_l'].append(pkg['source'])
                for s in SEVERITIES:
                    bugs_data[task][pkg['status']]['severities'][s] += bugs[pkg['source']]['severities'][s]
                    bugs_data[task]['weighttask'] += 1 * WEIGHT[s] * bugs[pkg['source']]['severities'][s]
            else:
                stderr.write("%s: Wrong status %s in task %s for source %s\n"
                             % (blendname, pkg['status'], task, pkg['source']))
                exit(1)
            bugs_data[task][pkg['status']]['sources'].append(sources)
            bugs_data[task]['nopenbugs'] += bugs[pkg['source']]['nopenbugs']
            bugs_data[task]['ndonebugs'] += bugs[pkg['source']]['ndonebugs']
            if bugs[pkg['source']]['done']:
                sources = {}
                sources['source']      = pkg['source']
                sources['homepage']    = pkg['homepage']
                sources['vcs_browser'] = pkg['vcs_browser']
                (_name, _url) = email.utils.parseaddr(pkg['maintainer'])
                sources['maintainer'] = {'name': _name, 'email': _url}
                sources['bugs']        = bugs[pkg['source']]['done']
                bugs_data[task]['done_l'].append(pkg['source'])
                bugs_data[task]['done']['sources'].append(sources)
    else:
        stderr.write("No information about buggy packages received for Blend %s\n" % blendname)
        exit(1)

    # Define directories used
    current_dir  = os.path.dirname(__file__)
    # locale_dir   = os.path.join(current_dir, 'locale')
    template_dir = os.path.join(current_dir, 'templates')

    # initialize gensi
    loader = TemplateLoader([template_dir], auto_reload=True,
                            default_encoding="utf-8")

    outputdir = CheckOrCreateOutputDir(config['outputdir'], 'bugs')  # FIXME: as long as we are not finished use different dir
    if outputdir is None:
        exit(-1)

    t = datetime.now()

    # Initialize i18n
    domain = 'blends-webtools'
    gettext.install(domain)

    data = {}
    data['projectname'] = blendname
    data['bugs_data']   = bugs_data
    if config.get('advertising') is not None:
        # we have to remove the gettext _() call which was inserted into the config
        # file to enable easy input for config file editors - but the call has to
        # be made explicitely in the python code
        advertising = re.sub('_\(\W(.+)\W\)', '\\1', config['advertising'])
        # gettext needs to escape '"' thus we need to remove the escape character '\'
        data['projectadvertising'] = Markup(re.sub('\\\\"', '"', advertising))
    else:
        data['projectadvertising'] = None

    data['summary']           = _('Summary')
    data['idxsummary']        = _("""A %sDebian Pure Blend%s is a Debian internal project which assembles
a set of packages that might help users to solve certain tasks of their work.  The list on
the right shows the tasks of %s.""") \
        % ('<a href="https://blends.debian.org/">', '</a>', data['projectname'])
    data['idxsummary']        = Markup(data['idxsummary'])

    t = datetime.now()
    data['lang']              = 'en'
    data['othertasks']        = _("Links to other tasks")
    data['taskslink']         = _("Tasks")
    data['bugslink']          = _("Tasks overview")
    data['legend']            = _("Legend")
    data['bugsoftask']        = _("Bugs of task")
    data['totalbugs']         = _("Total bugs")
    data['openbugs']          = _("Open bugs")
    data['fixedbugs']         = _("Fixed bugs")
    data['summary']           = _('Summary')
    data['bugssummary']       = _("""A %sDebian Pure Blend%s is a Debian internal project which assembles
    a set of packages that might help users to solve certain tasks of their work.  This page should be helpful
    to track down the bugs of packages that are interesting for the %s project to enable developers a quick
    overview about possible problems.""") \
        % ('<a href="https://blends.debian.org/">', '</a>', data['projectname'])
    data['bugssummary']        = Markup(data['bugssummary'])
    data['gtstrBugsPage']     = _("Bugs page")
    data['gtstrListOfBugspages'] = _("This is a list of metapackages.  The links are leading to the respective bugs page.")
    data['updatetimestamp']   = _('Last update:') + ' ' + formatdate(time.mktime(t.timetuple()))
    data['weightexplanation'] = _("""To estimate the overall status of the packages in the dependencies of
    a metapackage a weighted severity is calculated.  Done bugs are ignored and bugs in dependent and
    recommended packages are weighted by factor three compared to suggested packages.  Release critical
    bugs have a much larger weight than important, while the contribution of normal bugs is even smaller
    and minor bugs have a very small weight.  Wishlist bugs are ignored in this calculation.  The resulting
    sum is compared to some boundaries to find a verbal form.  The actual numbers need some adjustment
    to make real sense - this evaluation method is in testing phase.""")
    data['weightdetails']     = _("The severities of bugs are weighted as follows")

    data['assessments'] = [
        (EXCELLENT,    'excellent'),
        (VERYGOOD,     'verygood'),
        (GOOD,         'good'),
        (SATISFACTORY, 'satisfactory'),
        (PASS,         'pass'),
        (BAD,          'bad')
    ]

    for task in bugs_data:
        if bugs_data[task]['weighttask'] < data['assessments'][0][0]:
            bugs_data[task]['weightedsev']   = _('Task is in excellent shape')
            bugs_data[task]['weightedclass'] = data['assessments'][0][1]
        elif bugs_data[task]['weighttask'] < data['assessments'][1][0]:
            bugs_data[task]['weightedsev']   = _('Task is in very good shape')
            bugs_data[task]['weightedclass'] = data['assessments'][1][1]
        elif bugs_data[task]['weighttask'] < data['assessments'][2][0]:
            bugs_data[task]['weightedsev']   = _('Task is in good shape')
            bugs_data[task]['weightedclass'] = data['assessments'][2][1]
        elif bugs_data[task]['weighttask'] < data['assessments'][3][0]:
            bugs_data[task]['weightedsev']   = _('Consider looking into bugs of this task')
            bugs_data[task]['weightedclass'] = data['assessments'][3][1]
        elif bugs_data[task]['weighttask'] < data['assessments'][4][0]:
            bugs_data[task]['weightedsev']   = _('Looking into bugs of this task is recommended')
            bugs_data[task]['weightedclass'] = data['assessments'][4][1]
        else:
            bugs_data[task]['weightedsev']   = _('Immediately looking into bugs of the dependencies of this task is advised')
            bugs_data[task]['weightedclass'] = data['assessments'][5][1]
            bugs_data[task]['weightedsev']   += ' (%i)' % bugs_data[task]['weighttask']

    # Debuging output in JSON file
    if debug > 0:
        with open(blendname + '_bugs.json', 'w') as f:
            if debug > 1:
                for task in bugs_data:
                    f.write("*** %s ***\n" % task)
                    for status in STATES:
                        if status in bugs_data[task]:
                            f.write("%s\n" % status)
                            f.write(json.dumps(bugs_data[task][status]))
                            f.write("\n\n")
            f.write(json.dumps(bugs_data))
        SetFilePermissions(blendname + '_bugs.json')

    nbugs           = {}
    ndone           = {}
    buglist         = {}
    weightedsev     = {}  # verbal interpretation of weighted bugs
    weightedclass   = {}  # CSS class according bug weight
    weighttask      = {}  # weighted severity as number per task

    wsev = 0  # sumarise weighted severities
    for task in bugs_data:
        for status in STATES:
            if status != 'done':
                komma  = ''
                for s in SEVERITIES:
                    if bugs_data[task][status]['severities'][s] != 0:
                        bugs_data[task][status]['severitysummary'] += '%s %i %s' % (komma, bugs_data[task][status]['severities'][s], s)
                        komma = ','
        if wsev < data['assessments'][0][0]:
            weightedsev[task]   = _('Metapackage is in excellent shape')
            weightedclass[task] = data['assessments'][0][1]
        elif wsev < data['assessments'][1][0]:
            weightedsev[task]   = _('Metapackage is in very good shape')
            weightedclass[task] = data['assessments'][1][1]
        elif wsev < data['assessments'][2][0]:
            weightedsev[task]   = _('Metapackage is in good shape')
            weightedclass[task] = data['assessments'][2][1]
        elif wsev < data['assessments'][3][0]:
            weightedsev[task]   = _('Consider looking into bugs of this metapackage')
            weightedclass[task] = data['assessments'][3][1]
        elif wsev < data['assessments'][4][0]:
            weightedsev[task]   = _('Looking into bugs of this metapackage is recommended')
            weightedclass[task] = data['assessments'][4][1]
        else:
            weightedsev[task]   = _('Immediately looking into bugs of the dependencies of this metapackage is advised')
            weightedclass[task] = data['assessments'][5][1]
        weightedsev[task] += ' (%i)' % wsev
        weighttask[task]   = wsev

    data['headings'] = {
        'dependent' : _('Open bugs in dependent packages'),
        'suggested' : _('Open bugs in suggested packages'),
        'done'      : _('Done bugs')
    }
    data['nobugs']   = {
        'dependent' : _('No open bugs in dependent packages'),
        'suggested' : _('No open bugs in suggested packages'),
        'done'      : _('No done bugs')
    }
    data['cssclass'] = {
        'dependent' : 'bugsdependent',
        'suggested' : 'bugssuggested',
        'done'      : 'bugsdone'
    }
    # FIXME: just always use 'depends' or 'dependent' etc.  This translation is just to be able to compare with old output
    data['category'] = {
        'depends'   : 'dependent',
        'suggests'  : 'suggested',
        'done'      : 'done'
    }

    data['nbugs']           = nbugs
    data['ndone']           = ndone
    data['weight']          = WEIGHT
    data['severities']      = SEVERITIES
    data['states']          = STATES
    data['nohomepage']      = _('Homepage not available')
    data['novcsbrowser']    = _('Not maintained in Vcs')
    data['vcslocation']     = _('Vcs')

    data['weighttask']      = weighttask
    data['weightedclass']   = weightedclass

    for key in ('css', 'homepage', 'projecturl', 'projectname', 'logourl', 'ubuntuhome', 'projectubuntu'):
        data[key] = config[key]

    for task in bugs_data:
        data['task']            = task
        # data['buglist']         = buglist[task]
        # data['weightedsev']     = weightedsev[task]
        # data['severitystat']    = severitystat[task]

        template = loader.load('bugs.xhtml')
        with codecs.open(outputdir + '/' + task + '.html', 'w', 'utf-8') as f:
            f.write(template.generate(**data).render('xhtml'))
        SetFilePermissions(outputdir + '/' + task + '.html')

    template = loader.load('bugs_idx.xhtml')
    outputfile = outputdir + '/index.html'
    with codecs.open(outputfile, 'w', 'utf-8') as f:
        f.write(template.generate(**data).render('xhtml'))
    SetFilePermissions(outputfile)

if __name__ == '__main__':
    main()
