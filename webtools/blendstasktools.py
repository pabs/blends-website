''' Blends metapackages are listing a set of Dependencies
These might be fullfilled by the Debian package
set or not.

This interface provides some classes that contains
all available information about this Dependency ,
like whether it is an official package or not,
in which distribution it is contained
or if it is not contained it obtains information
from tasks file about home page, license, WNPP etc.
Copyright 2008-2012: Andreas Tille <tille@debian.org>
License: GPL
'''

from sys import stderr, exit
import itertools
from subprocess import Popen, PIPE
import codecs
import os
import grp
import stat
import gzip
import bz2
import re
import email.utils

import psycopg2
import gettext

from blendsmarkdown import SplitDescription, PrepareMarkdownInput

try:
    from debian import deb822
except:
    from debian_bundle import deb822
from blendslanguages import languages

import logging
import logging.handlers
logger = logging.getLogger('blends')
logger.setLevel(logging.INFO)
# logger.setLevel(logging.DEBUG)

# PORT = 5441
UDDPORT = 5452
PORT = UDDPORT
DEFAULTPORT = 5432

# Seems to have problems on 17.04.2009
# BASEURL  = 'https://ftp.debian.org/debian'
BASEURL = 'https://ftp.de.debian.org/debian'
KEYSVALID = ('Depends', 'Recommends', 'Suggests')
KEYSTOIGNORE = ('Architecture', 'Comment', 'Leaf', 'NeedConfig', 'Note', 'Section',
                'Needconfig', 'DontAvoid',
                'Enhances', 'Test-always-lang', 'Metapackage')

CONFDIR = 'webconf'

COMPRESSIONEXTENSION = 'bz2'
# COMPRESSIONEXTENSION = 'gz' # Translations are only available as bz2 since April 2009

HOMEPAGENONE = '#'
HOMEPAGENONEFIELDS = (
    'homepage',
    'pkg-url',      # Link to packages.debian.org search interface with exact
                    # package matches or URL to inofficial package
)

PKGURLMASK = 'https://packages.debian.org/search?keywords=%s&searchon=names&exact=1&suite=all&section=all'

DEPENDENT  = 0
SUGGESTED  = 1
DONE       = 2
BUGLISTCAT = (DEPENDENT, SUGGESTED, DONE)

# FIXME: Obtain releases from UDD table releases (is this used at all???)
releases = {
    'oldstable'   : ('wheezy', 'wheezy-proposed-updates', 'wheezy-security'),
    'stable'      : ('jessie', 'jessie-proposed-updates', 'jessie-security'),
    'testing'     : ('stretch'),
    'unstable'    : ('sid'),
    'experimental': ('experimental')
}

pkgstatus = {
    'official_high': {  # official package with high priority dependency
        'releases'     : ('oldstable', 'stable', 'testing', 'unstable'),
        'components'   : ('main', ),
        'dependencies' : ('Depends', 'Recommends'),
        'fields-set'   : (),
        'colorcode'    : 'Green: The project is <a href="#%s">available as an official Debian package and has high relevance</a>',
        'order'        : 1
    },
    'official_low': {  # official package with low priority dependency
        'releases'     : ('oldstable', 'stable', 'testing', 'unstable'),
        'components'   : ('main', ),
        'dependencies' : ('Suggests', ),
        'fields-set'   : (),
        'colorcode'    : 'Green: The project is <a href="#%s">available as an official Debian package but has lower relevance</a>',
        'order'        : 2
    },
    'non-free': {  # package in contrib or non-free, priority decreased to Suggests in any case
        'releases'     : ('oldstable', 'stable', 'testing', 'unstable'),
        'component'    : ('contrib', 'non-free'),
        'dependencies' : ('Depends', 'Recommends', 'Suggests'),
        'fields-set'   : (),
        'colorcode'    : 'Green: The project is <a href="#%s">available in Debian packaging pool but is not in Debian main</a>',
        'order'        : 3
    },
    'experimental': {  # package which exists only in experimental
        'releases'     : ('experimental', ),
        'component'    : ('main', 'contrib', 'non-free'),
        'dependencies' : ('Depends', 'Recommends', 'Suggests'),
        'fields-set'   : (),
        'colorcode'    : 'Yellow: The project is <a href="#%s">available in Debian packaging pool but is regarded as experimental</a>',
        'order'        : 4
    },
    'new': {  # package in new queue
        'releases'     : ('new', ),
        'component'    : ('main', 'contrib', 'non-free'),
        'dependencies' : ('Depends', 'Recommends', 'Suggests'),
        'fields-set'   : (),
        'colorcode'    : 'Yellow: A package of project is <a href="#%s">is in Debian New queue and hopefully available soon</a>',
        'order'        : 5
    },
    'pkgvcs': {  # Not yet packaged but packaging code in Vcs
        'releases'     : (),
        'component'    : (),
        'dependencies' : ('Depends', 'Recommends', 'Suggests'),
        'fields-set'   : (),
        'colorcode'    : 'Yellow: The packaging of project is <a href="#%s">has started and a developer might try the packaging code in VCS or help packaging.</a>',
        'order'        : 6
    },
    'unofficial': {  # unofficial packages outside Debian
        'releases'     : (),
        'component'    : (),
        'dependencies' : ('Depends', 'Recommends', 'Suggests'),
        'fields-set'   : ('pkg-url', ),
        'colorcode'    : 'Yellow: There exists an <a href="#%s">unofficial package</a> of the project',
        'order'        : 7
    },
    'wnpp': {  # project which has at least a WNPP bug filed
        'releases'     : (),
        'component'    : (),
        'dependencies' : ('Depends', 'Recommends', 'Suggests'),
        'fields-set'   : ('wnpp', ),
        'colorcode'    : 'Red: The project is <a href="#%s">not (yet) available as a Debian package</a> but there is some record of interest (WNPP bug).',
        'order'        : 8
    },
    'prospective': {  # projects which might be interesting for a Blend but no work is done yet
        'releases'     : (),
        'component'    : (),
        'dependencies' : ('Depends', 'Recommends', 'Suggests'),
        'fields-set'   : ('homepage', ),  # TODO: a description should be set as well ...
        'colorcode'    : 'Red: The project is <a href="#%s">not (yet) available as a Debian package</a>.',
        'order'        : 9
    },
    'ignore': {  # Package inside Debian which is "under observation"
        'releases'     : (releases.keys()),
        'component'    : ('main', 'contrib', 'non-free'),
        'dependencies' : ('Ignore', ),
        'fields-set'   : (),
        'colorcode'    : '%s',
        'order'        : 10
    },
    'avoid': {  # Package inside Debian which should not go to a install medium of the Blend
        'releases'     : (releases.keys()),
        'component'    : ('main', 'contrib', 'non-free'),
        'dependencies' : ('Avoid', ),
        'fields-set'   : (),
        'colorcode'    : '%s',
        'order'        : 11
    },
    'unknown': {  # Everything else
        'releases'     : (),
        'component'    : (),
        'dependencies' : ('Depends', 'Recommends', 'Suggests'),
        'fields-set'   : (),
        'colorcode'    : 'White: The project has an %s status.',
        'order'        : 100
    },
}

pkgstatus_sortedkeys = sorted(pkgstatus.keys(),
                              key=lambda x: (pkgstatus[x]['order'], x))

dep_strength_keys = set(itertools.chain(*(pkgstatus[pkgstat]['dependencies']
                                          for pkgstat in pkgstatus)))

rmpub = codecs.open('remove-publications-from-tasks-files.dat', 'w+', 'utf-8')

license_in_component = {
    'main'    : 'DFSG free',
    'contrib' : 'DFSG free, but needs non-free components',
    'non-free': 'non-free'
}

try:
    import psutil
    has_psutils = True
except ImportError:
    has_psutils = False


def SetFilePermissions(usefile):
    try:
        blendsgid = grp.getgrnam("blends").gr_gid
        os.chown(usefile, -1, blendsgid)
        os.chmod(usefile, stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP |
                 stat.S_IWGRP | stat.S_IROTH)
        # os.system("ls -l %s" % usefile)
    except KeyError:
        # if groups 'blends' does not exist on the machine we are simply testing and setting group permissions is not needed
        pass


LOCKFILE = '/var/lock/blends.lock'
def LockBlendsTools():
    """Locking mechanism to make sure the scripts will not run in parallel
       which happened because of IO problems on udd.debian.org"""
    if not has_psutils:
        logger.warning("Package python-psutil is missing.  No locking support available")
        return
    if os.path.exists(LOCKFILE):
        try:
            with open(LOCKFILE, 'r') as lf:
                pid = int(lf.readline())
            try:  # psutils has changed interfacde and get_pid_list() does not exist any more in newer implementations
                pidlist = psutil.get_pid_list()
            except AttributeError:
                pidlist = psutil.pids()
            if pid in pidlist:
                logger.error("Another process rebuilding web sentinel pages with PID %i is running. Exit." % pid)
                exit()
            else:
                logger.warning("Process with PID %i is not running any more but lockfile remained.  Removing %s ..." % (pid, LOCKFILE))
                os.unlink(LOCKFILE)
        except IOError as e:
            pass
    with open(LOCKFILE, 'w') as lf:
        lf.write('%i\n' % os.getpid())
    SetFilePermissions(LOCKFILE)


def UnlockBlendsTools():
    """Unlock previousely locked file"""
    if os.path.exists(LOCKFILE):
        os.unlink(LOCKFILE)


def GetDependencies2Use(dependencystatus=[], max_order='prospective'):
    # Create a list of status of dependencies out of pkgstatus dictionary
    use_dependencystatus = []
    if dependencystatus == []:
        for pkgstat in pkgstatus_sortedkeys:
            # per default = if no expliczite dependencystatus are given, we are only interested in
            # dependencies which are at least of order experimental
            if pkgstatus[pkgstat]['order'] > pkgstatus[max_order]['order']:
                continue
            use_dependencystatus.append(pkgstat)
    else:
        # verify correctly given dependencies
        for pkgstat in dependencystatus:
            if pkgstat in pkgstatus:
                use_dependencystatus.append(pkgstat)
            else:
                logger.error("Unknown dependencystatus %s" % pkgstat)
        if use_dependencystatus == []:
            logger.error("No valid dependencystatus in", dependencystatus)
    return use_dependencystatus

###############################################################################
connection_pars = [
    {'host': 'localhost', 'port': PORT, 'user': 'guest', 'database': 'udd'},
    {'service': 'udd'},
    {'host': 'localhost', 'port': DEFAULTPORT, 'user': 'guest', 'database': 'udd'},

    # Hmmm, I observed a really strange behaviour on one of my machines where
    # connecting to localhost does not work but 127.0.0.1 works fine.  No idea
    # why ... but this should do the trick for the moment
    {'host': '127.0.0.1', 'port': DEFAULTPORT, 'user': 'guest', 'database': 'udd'},

    # Public UDD mirror as last resort
    {'host': 'public-udd-mirror.xvm.mit.edu', 'port': 5432,
     'user': 'public-udd-mirror', 'password': 'public-udd-mirror', 'database': 'udd'},
]
conn = None
for par in connection_pars:
    try:
        conn = psycopg2.connect(**par)
        break
    except psycopg2.OperationalError as err:
        continue

if conn is None:
    raise err

conn.set_client_encoding('utf-8')
curs = conn.cursor()
# uddlog = open('logs/uddquery.log', 'w')


def _execute_udd_query(query):
    try:
        curs.execute(query)
        logger.debug(query)
    except psycopg2.ProgrammingError as err:
        stderr.write("Problem with query\n%s\n%s\n" % (query, str(err)))
        exit(-1)
    except psycopg2.DataError as err:
        stderr.write("%s; query was\n%s\n" % (str(err), query))

# Define several prepared statements to query UDD
query = """PREPARE query_pkgs (text[],text[],text) AS
        SELECT * FROM blends_query_packages($1,$2,$3) AS (
          package text, distribution text, release text, component text, version debversion,
          maintainer text,
          source text, section text, task text, homepage text,
          maintainer_name text, maintainer_email text,
          "vcs-type" text, "vcs-url" text, "vcs-browser" text,
          changed_by text,
          enhanced text[],
          releases text[], versions text[], architectures text[],
          unstable_upstream text, unstable_parsed_version text, unstable_status text,
          vote int, recent int, insts int, -- popcon
          debtags text[],
          screenshot_versions text[], image text[], icon text[],
          year    text,
          title   text,
          authors text,
          doi     text,
          pubmed  text,
          url     text,
          journal text,
          volume  text,
          number  text,
          pages   text,
          eprint  text,
          description_en text, long_description_en text,
          description_cs text, long_description_cs text,
          description_da text, long_description_da text,
          description_de text, long_description_de text,
          description_es text, long_description_es text,
          description_fi text, long_description_fi text,
          description_fr text, long_description_fr text,
          description_hu text, long_description_hu text,
          description_it text, long_description_it text,
          description_ja text, long_description_ja text,
          description_ko text, long_description_ko text,
          description_nl text, long_description_nl text,
          description_pl text, long_description_pl text,
          "description_pt_BR" text, "long_description_pt_BR" text,
          description_ru text, long_description_ru text,
          description_sk text, long_description_sk text,
          description_sr text, long_description_sr text,
          description_sv text, long_description_sv text,
          description_uk text, long_description_uk text,
          description_vi text, long_description_vi text,
          "description_zh_CN" text, "long_description_zh_CN" text,
          "description_zh_TW" text, "long_description_zh_TW" text,
          remark text,
          "upstream_bugs" text,
          "upstream_repository" text,
          "edam_topics" text[],
          biotools  text,
          omictools text,
          bioconda  text,
          scicrunch text,
          rrid      text,
          bugs      text
        )"""
_execute_udd_query(query)

query = """PREPARE query_new (text[]) AS SELECT
                   p.package,
                   p.distribution, p.component, p.version, p.architecture, p.maintainer,
                   p.source, p.section, p.distribution, 'new' AS release, p.component, p.homepage,
                   s.changed_by,
                   description AS description_en, long_description AS long_description_en,
                   s.vcs_url     AS "vcs-url",
                   s.vcs_type    AS "vcs-type",
                   s.vcs_browser AS "vcs-browser",
         bibyear.value    AS "year",
         bibtitle.value   AS "title",
         bibauthor.value  AS "authors",
         bibdoi.value     AS "doi",
         bibpmid.value    AS "pubmed",
         biburl.value     AS "url",
         bibjournal.value AS "journal",
         bibvolume.value  AS "volume",
         bibnumber.value  AS "number",
         bibpages.value   AS "pages",
         bibeprint.value  AS "eprint"
                   FROM new_packages p
                   JOIN new_sources s ON p.source = s.source AND p.version = s.version
    LEFT OUTER JOIN bibref bibyear    ON p.source = bibyear.source    AND bibyear.rank = 0    AND bibyear.key    = 'year'    AND bibyear.package = ''
    LEFT OUTER JOIN bibref bibtitle   ON p.source = bibtitle.source   AND bibtitle.rank = 0   AND bibtitle.key   = 'title'   AND bibtitle.package = ''
    LEFT OUTER JOIN bibref bibauthor  ON p.source = bibauthor.source  AND bibauthor.rank = 0  AND bibauthor.key  = 'author'  AND bibauthor.package = ''
    LEFT OUTER JOIN bibref bibdoi     ON p.source = bibdoi.source     AND bibdoi.rank = 0     AND bibdoi.key     = 'doi'     AND bibdoi.package = ''
    LEFT OUTER JOIN bibref bibpmid    ON p.source = bibpmid.source    AND bibpmid.rank = 0    AND bibpmid.key    = 'pmid'    AND bibpmid.package = ''
    LEFT OUTER JOIN bibref biburl     ON p.source = biburl.source     AND biburl.rank = 0     AND biburl.key     = 'url'     AND biburl.package = ''
    LEFT OUTER JOIN bibref bibjournal ON p.source = bibjournal.source AND bibjournal.rank = 0 AND bibjournal.key = 'journal' AND bibjournal.package = ''
    LEFT OUTER JOIN bibref bibvolume  ON p.source = bibvolume.source  AND bibvolume.rank = 0  AND bibvolume.key  = 'volume'  AND bibvolume.package = ''
    LEFT OUTER JOIN bibref bibnumber  ON p.source = bibnumber.source  AND bibnumber.rank = 0  AND bibnumber.key  = 'number'  AND bibnumber.package = ''
    LEFT OUTER JOIN bibref bibpages   ON p.source = bibpages.source   AND bibpages.rank = 0   AND bibpages.key   = 'pages'   AND bibpages.package = ''
    LEFT OUTER JOIN bibref bibeprint  ON p.source = bibeprint.source  AND bibeprint.rank = 0  AND bibeprint.key  = 'eprint'  AND bibeprint.package = ''
                   WHERE (p.package, p.version) IN
                         (SELECT package, max(version) FROM
                   new_packages WHERE package = ANY ($1) GROUP BY package) ORDER BY p.package"""
_execute_udd_query(query)

query = """PREPARE query_vcs (text[]) AS SELECT
                   p.package,
                   p.component, p.maintainer,
                   p.source, p.section, 'vcs' AS release, p.component, p.homepage,
                   p.changed_by, p.chlog_version AS version,
                   description AS description_en, long_description AS long_description_en,
                   p.vcs_url     AS "vcs-url",
                   p.vcs_type    AS "vcs-type",
                   p.vcs_browser AS "vcs-browser",
                   p.blend,
                   p.license,
                   p.wnpp,
         bibyear.value    AS "year",
         bibtitle.value   AS "title",
         bibauthor.value  AS "authors",
         bibdoi.value     AS "doi",
         bibpmid.value    AS "pubmed",
         biburl.value     AS "url",
         bibjournal.value AS "journal",
         bibvolume.value  AS "volume",
         bibnumber.value  AS "number",
         bibpages.value   AS "pages",
         bibeprint.value  AS "eprint"
                   FROM blends_prospectivepackages p
    LEFT OUTER JOIN bibref bibyear    ON p.source = bibyear.source    AND bibyear.rank = 0    AND bibyear.key    = 'year'    AND bibyear.package = ''
    LEFT OUTER JOIN bibref bibtitle   ON p.source = bibtitle.source   AND bibtitle.rank = 0   AND bibtitle.key   = 'title'   AND bibtitle.package = ''
    LEFT OUTER JOIN bibref bibauthor  ON p.source = bibauthor.source  AND bibauthor.rank = 0  AND bibauthor.key  = 'author'  AND bibauthor.package = ''
    LEFT OUTER JOIN bibref bibdoi     ON p.source = bibdoi.source     AND bibdoi.rank = 0     AND bibdoi.key     = 'doi'     AND bibdoi.package = ''
    LEFT OUTER JOIN bibref bibpmid    ON p.source = bibpmid.source    AND bibpmid.rank = 0    AND bibpmid.key    = 'pmid'    AND bibpmid.package = ''
    LEFT OUTER JOIN bibref biburl     ON p.source = biburl.source     AND biburl.rank = 0     AND biburl.key     = 'url'     AND biburl.package = ''
    LEFT OUTER JOIN bibref bibjournal ON p.source = bibjournal.source AND bibjournal.rank = 0 AND bibjournal.key = 'journal' AND bibjournal.package = ''
    LEFT OUTER JOIN bibref bibvolume  ON p.source = bibvolume.source  AND bibvolume.rank = 0  AND bibvolume.key  = 'volume'  AND bibvolume.package = ''
    LEFT OUTER JOIN bibref bibnumber  ON p.source = bibnumber.source  AND bibnumber.rank = 0  AND bibnumber.key  = 'number'  AND bibnumber.package = ''
    LEFT OUTER JOIN bibref bibpages   ON p.source = bibpages.source   AND bibpages.rank = 0   AND bibpages.key   = 'pages'   AND bibpages.package = ''
    LEFT OUTER JOIN bibref bibeprint  ON p.source = bibeprint.source  AND bibeprint.rank = 0  AND bibeprint.key  = 'eprint'  AND bibeprint.package = ''
                   WHERE p.package = ANY ($1) ORDER BY p.package"""
_execute_udd_query(query)

# This prepared statement is called only once but it makes sense to mention it in the
# header to keep all stuff in one place which needs to be changed in case a new language
# will be added
query = """PREPARE query_metapkg_trans (text[]) AS
        SELECT * FROM blends_metapackage_translations($1) AS (
          package text,
          description_en text, long_description_en text,
          description_cs text, long_description_cs text,
          description_da text, long_description_da text,
          description_de text, long_description_de text,
          description_es text, long_description_es text,
          description_fi text, long_description_fi text,
          description_fr text, long_description_fr text,
          description_hu text, long_description_hu text,
          description_it text, long_description_it text,
          description_ja text, long_description_ja text,
          description_ko text, long_description_ko text,
          description_nl text, long_description_nl text,
          description_pl text, long_description_pl text,
          "description_pt_BR" text, "long_description_pt_BR" text,
          description_ru text, long_description_ru text,
          description_sk text, long_description_sk text,
          description_sr text, long_description_sr text,
          description_sv text, long_description_sv text,
          description_uk text, long_description_uk text,
          description_vi text, long_description_vi text,
          "description_zh_CN" text, "long_description_zh_CN" text,
          "description_zh_TW" text, "long_description_zh_TW" text
        )"""
_execute_udd_query(query)

# Sometimes the tasks file contains dependencies from virtual packages and we have to
# obtain the real packages which provide this dependency.
# First check whether there are such packages (only names)
query = """PREPARE query_provides (text[]) AS
           SELECT DISTINCT package, provides FROM packages WHERE provides IS NOT NULL AND package = ANY($1) ;"""
_execute_udd_query(query)

# Obtain more detailed information about packages that might provide a dependency
# query = """PREPARE query_provides_version_release (text) AS
#           SELECT package, version, release FROM packages WHERE provides = $1
#                  GROUP BY version, package, release ORDER BY version DESC;"""
# _execute_udd_query(query)

# Obtain the releases featuring a certain package, in case a package might show up in different components when
# considering different releases we apply a preference for main over contrib over non-free.  If this is the case
# we mention only the releases of the selected component
query = "PREPARE pkg_releases (text, text) AS SELECT release FROM packages WHERE package = $1 AND component = $2 GROUP BY release "
_execute_udd_query(query)

query = """PREPARE pkg_versions_stable_testing (text) AS
   SELECT release,  regexp_replace(regexp_replace(debversion, '-.*', ''), '[.+~]dfsg.*', '') AS version, debversion
     FROM (SELECT r AS release, MAX(version) AS debversion
             FROM versions_archs_component($1) AS (r text, version text, archs text, component text)
            WHERE r IN ('jessie', 'stretch') GROUP BY r) AS zw;"""  # FIXME: Do not hardcode names here!!!
_execute_udd_query(query)

# Number of all submissions
query = "PREPARE popcon_submissions AS SELECT vote FROM popcon WHERE package = '_submissions'"
_execute_udd_query(query)

#########################################################################################


def List2PgArray(l):
    # turn a list of strings into the syntax for a PostgreSQL array:
    # {"string1","string2",...,"stringN"}
    return '{' + ','.join(('"' + s + '"') for s in l) + '}'


def List2PgSimilarArray(l):
    # turn a list of strings surrounded by '%' into the syntax for a PostgreSQL array to enable LIKE conditions:
    # {"%string1%","%string2%",...,"%stringN%"}
    return '{' + ','.join(('"%' + s + '%"') for s in l) + '}'


def ReadConfig(blendname=''):
    # Try to read config file CONFDIR/<blendname>.conf
    conffile = CONFDIR + '/' + blendname + '.conf'
    if not os.access(conffile, os.R_OK):
        # if config file can not be found in local dir, try /etc/blends/webconf as fallback
        conffile_default = '/etc/blends/webconf/' + blendname + '.conf'
        if not os.access(conffile_default, os.R_OK):
            logger.error("Unable to open config file %s or %s."
                         % (conffile, conffile_default))
            exit(-1)
        conffile = conffile_default
    f = open(conffile, 'r')
    ret = {
        'Blend'        : '',
        'projectname'  : '',
        'projecturl'   : '',
        'homepage'     : '',
        'aliothurl'    : '',
        'projectlist'  : '',
        'pkglist'      : '',
        'logourl'      : '',
        'css'          : '',
        'outputdir'    : '',
        'datadir'      : '',
        'advertising'  : None,  # headline with advertising string is optional
        'ubuntuhome'   : None,
        'projectubuntu': None,
    }
    for stanza in deb822.Sources.iter_paragraphs(f, shared_storage=False):
        ret['Blend']       = stanza['blend']        # short name of the project
        ret['projectname'] = stanza['projectname']  # Printed name of the project
        ret['projecturl']  = stanza['projecturl']   # Link to the developer page with dynamic content
                                                    # like for instance these tasks pages
        ret['homepage']    = stanza['homepage']     # Homepage with general information about the project
                                                    # for instance at www.debian.org or wiki.debian.org
        ret['aliothurl']   = stanza['aliothurl']    # Link to the Alioth page of the project
        ret['projectlist'] = stanza['projectlist']  # Mailinglist of the project
        if 'pkglist' in stanza:
            ret['pkglist'] = stanza['pkglist']      # Packaging Mailinglist = Maintainer of group maintained packages
        if 'logourl' in stanza:
            ret['logourl'] = stanza['logourl']      # URL to logo image (might be missing
        ret['css']         = stanza['css']          # (relative) URL to CSS file
        ret['outputdir']   = stanza['outputdir']    # Dir for storing output HTML files
        ret['datadir']     = stanza['datadir']      # Dir for storing SVN information about project
        ret['vcsdir']      = stanza['vcsdir']       # Path to Blend information files at svn.debian.org
        if 'advertising' in stanza:
            # we have to remove the gettext _() call which was inserted into the config
            # file to enable easy input for config file editors - but the call has to
            # be made explicitely in the python code
            advertising = re.sub('_\(\W(.+)\W\)', '\\1', stanza['advertising'])
            # gettext needs to escape '"' thus we need to remove the escape character '\'
            ret['advertising'] = re.sub('\\\\"', '"', advertising)
        if 'ubuntuhome' in stanza:
            ret['ubuntuhome'] = stanza['ubuntuhome']
        if 'projectubuntu' in stanza:
            ret['projectubuntu'] = stanza['projectubuntu']

    return ret


def CheckOrCreateOutputDir(maindir, subdir):
    outputdir = maindir + '/' + subdir
    if not os.access(outputdir, os.W_OK):
        try:
            os.mkdir(outputdir)
        except:
            # if more than one dir in the tree has to be crated just use mkdir -p ...
            try:
                os.system("mkdir -p %s" % outputdir)
            except:
                logger.error("Unable to create output dir %s" % outputdir)
                return None
    return outputdir


def FetchTasksFiles(data):
    # Fetch tasks files from SVN of a Blend
    # The specification of the repository containing the tasks files
    # of a Blend can be done in webconf/<Blend>.conf

    # tasks directory to obtain dependencies and debian/control to obtain meta
    # information like the metapackage prefix
    # Checkout/Update tasks from SVN
    if data['vcsdir'].startswith('svn:'):
        for dir in ('tasks', 'debian'):
            tasksdir = os.path.join(data['datadir'], dir)
            if not os.access(tasksdir, os.W_OK):
                try:
                    os.makedirs(tasksdir)
                except:
                    logger.error("Unable to create data directory", tasksdir)
            svncommand = "svn %%s %%s %s >> /dev/null" % tasksdir
            if os.path.isdir(os.path.join(tasksdir, '.svn')):
                svncommand = svncommand % (' --accept theirs-conflict up', '')
            else:
                os.system("mkdir -p %s" % (tasksdir))
                svncommand = svncommand % ('co', os.path.join(data['vcsdir'], dir))
            if os.system(svncommand):
                logger.error("SVN command %s failed" % svncommand)
                if os.path.isdir(os.path.join(tasksdir, '.svn')):
                    logger.error("Trying old files in %s ..." % tasksdir)
                else:
                    if os.listdir(tasksdir):
                        logger.warning("No .svn directory found in %s but trying those random files there as tasks files." % tasksdir)
                    else:
                        logger.error("There are no old files in %s -> giving up" % tasksdir)
                        exit(-1)
    elif data['vcsdir'].startswith('git:') or data['vcsdir'].startswith('http'):
        githtml = data['vcsdir']
#        if githtml.startswith('git:'):
#            githtml=githtml.replace('git://','http://')
        if os.path.isdir(os.path.join(data['datadir'], '.git')):
            gitcommand = "cd %s; git pull --quiet" % data['datadir']
        else:
            gitcommand = "git clone --quiet %s %s" % (githtml, data['datadir'])
        if os.system(gitcommand):
            logger.error("Git command %s failed" % (gitcommand))
            if os.path.isdir(os.path.join(data['datadir'], '.git')):
                logger.error("Trying old files in %s ..." % data['datadir'])
            else:
                if os.listdir(data['datadir']):
                    logger.warning("No .git directory found in %s but trying those random files there as tasks files." % data['datadir'])
                else:
                    logger.error("There are no old files in %s -> giving up" % data['datadir'])
                    exit(-1)
    else:
        logger.error("Don't know how to checkout tasks files from %s -> giving up"
                     % data['vcsdir'])
        exit(-1)
    return os.path.join(data['datadir'], 'tasks')


def RowDictionaries(cursor):
    """Return a list of dictionaries which specify the values by their column names"""

    if not cursor.description:
        # even if there are no data sets to return the description
        # should contain the table structure.  If not something went
        # wrong and we return None as to represent a problem
        return None

    try:
        return [dict((dd[0], dv) for (dd, dv) in zip(cursor.description, row))
                for row in cursor]
    
    except NameError: # Python 3
        return [dict((dd[0], dv) for (dd, dv) in zip(cursor.description, row))
                for row in cursor]

def BrowserFromVcsURL(vcs_type, vcs_url):
    # Guess Vcs-Browser URL from VCS URL
    if not vcs_type:
        return HOMEPAGENONE
    if vcs_type.lower().startswith('svn'):
        ret_url = re.sub('^svn:', 'https:', vcs_url)
        ret_url = re.sub('/svn/', '/wsvn/', ret_url)
        ret_url = re.sub('$', '?rev=0&sc=0', ret_url)
    elif vcs_type.lower().startswith('git'):
        ret_url = re.sub('^git:', 'https:', vcs_url)
        ret_url = re.sub('/git/', '/?p=', ret_url)
    elif vcs_type.lower().startswith('hg'):
        # Seems that vcs_browser = vcs_url in Mercurial
        return vcs_url
    elif vcs_type.lower().startswith('bzr') or vcs_type.lower().startswith('cvs'):
        logger.warning("No idea how to guess vcs_browser for %s URLS" % vcs_type)
        return
    else:
        logger.warning("Unknown VCS for " + vcs_url)
        return HOMEPAGENONE
    if vcs_url.startswith('https://github.com') and vcs_url.endswith('.git'):
        ret_url = re.sub('.git$', '', vcs_url)
    elif vcs_url.startswith('http:'):
        return vcs_url
    if ret_url == vcs_url:
        logger.warning("Unable to obtain Vcs-Browser from " + vcs_url)
        return HOMEPAGENONE
    return ret_url

detect_vcs_cvs_re        = re.compile("://.*cvs")
detect_vcs_svn_re        = re.compile("://.*svn")
detect_vcs_git_re        = re.compile("://.*git")
detect_vcs_salsa_re      = re.compile("://.*salsa")

def VcsTypeFromBrowserURL(vcs_browser):
    # Guess Vcs-Type from vcs_browser
    if detect_vcs_cvs_re.search(vcs_browser):
        return 'Cvs'
    if detect_vcs_svn_re.search(vcs_browser):
        return 'Svn'
    if detect_vcs_git_re.search(vcs_browser):
        return 'Git'
    if detect_vcs_salsa_re.search(vcs_browser):
        return 'Git'
    return 'Unknown VCS'

# The following keys will be mostly used for programs that
# are not yet existing in Debian and will go to our todo list
PROPERTIES = (
    'homepage',  # Homepage of program
    'section',   # Section of package in the Debian hierarchy
    'source',    # Keep the source package name which is needed for ddpo subscription
)


class DependantPackage:
    # Hold information about a package that is in dependency list

    def __init__(self, name):
        self.properties     = {}
        self.properties['name'] = name
        self.properties['license']     = 'unknown'
        self.properties['pkgstatus']     = 'unknown'
        self.properties['component']     = None
        for field in HOMEPAGENONEFIELDS:
            self.properties[field]    = HOMEPAGENONE
        self.properties['Enhances'] = {}  # Dictionary Enhancing pkg name as key, Link to package information as value; empty in most cases
                                          # because Enhances relations are quite seldom
        self.properties['stable_testing_version'] = []  # (release, version) tuples where release is codename for stable and testing
        self.properties['version'] = []  # list of {'release', 'version', 'archs'} dictionary containing version and architecture information
        self.properties['desc'] = {'en': {}}  # An English description should be available in any case

    def __str__(self):
        ret = "pkg:"    + self.properties['name']
        for prop in self.properties:
            ret += ", %s: %s" % (prop, str(self.properties[prop]))

        # if self.desc['en']:
        #    ret += ", desc['en']:"   + str(self.desc['en'])
        return ret

def setPublications(dep, taskname, row):
    for pub in ("year", "title", "authors", "doi", "pubmed", "url",
                "journal", "volume", "number", "pages", "eprint"):
        if row[pub]:
            if pub == "pages":
                row[pub] = re.sub("--", "-", row[pub])
            if (pub == "authors" and row[pub].count(" and ") or row[pub].count(" AND ")):
                # assume "authors" string is a regular BibTeX "and" separated list of authors
                row[pub] = re.sub("AND", "and", row[pub].strip())
                authors_list = row[pub].split(" and ")
                # normalize several BibTeX styles to "First Last, First Last and First Last":
                # 1. "First Last and First Last and First Last"
                # 2. "Last, First and Last, First and Last, First"
                # 3. "First Last, First Last and First Last"
                authors_string = ""
                while (authors_list):
                    author = authors_list.pop(0)
                    if (author.count(",") > 1):
                        # 3. "First Last, First Last and First Last"
                        # authors string is already in desired format, keep it
                        authors_string = row[pub].strip()
                        break
                    elif (row[pub].count(",") == row[pub].count(" and ") + 1):
                        # 2. "Last, First and Last, First and Last, First"
                        # reverse last and first name
                        (last, first) = author.split(", ")
                        full_author = first + " " + last
                    else:
                        # 1. "First Last and First Last and First Last"
                        full_author = author
                    if (len(authors_list) > 1):
                        authors_string += full_author + ", "
                    elif (len(authors_list) > 0):
                        authors_string += full_author + " and "
                    else:
                        authors_string += full_author
                if row[pub] != authors_string:
                    # emergency brake if algorithm fails to detect non-names like '1000 Genome Project Data Processing Subgroup'
                    if authors_string.count(',') > row[pub].count(' and '):
                        logger.warning("Refuse to change Author string in %s: '%s'(%i) -> '%s'(%i)"
                                       % (dep.properties['name'], row[pub], row[pub].count(' and '), authors_string, authors_string.count(',')))
                    else:
                        logger.debug("Author string changed in %s: '%s' -> '%s'"
                                     % (dep.properties['name'], row[pub], authors_string))
                        row[pub] = authors_string
            if 'published' not in dep.properties:
                dep.properties['published'] = {}
            if pub in dep.properties['published']:
                if dep.properties['published'][pub] == row[pub]:
                    rmpub.write("%s: %s: Published-%s: %s" % (taskname, dep.properties['name'], pub, row[pub]))
                    logger.info("%s/%s: Publication-%s = %s can be removed"  % (taskname, dep.properties['name'], pub, row[pub]))
                else:
                    logger.info("%s conflicting fields Publication-%s in tasks file with value '%s' and in UDD with value '%s'" % (dep.properties['name'], pub, dep.properties['published'][pub], row[pub]))
            dep.properties['published'][pub] = row[pub]


class Tasks:
    # Provide a list of depencencies defined in metapackages
    # This class concerns _all_ tasks of a Blend and is the most
    # complete source of information.  If only a single task
    # should be handled by a tool that uses blendtasktools
    # probably the class TaskDependencies (see below) is
    # your friend

    def __init__(self, blendname):

        os.system("mkdir -p logs")
        LOG_FILENAME = os.path.join('logs', blendname + '.log')
        handler = logging.handlers.RotatingFileHandler(filename=LOG_FILENAME, mode='w')
        formatter = logging.Formatter("%(levelname)s - %(filename)s (%(lineno)d): %(message)s")
        handler.setFormatter(formatter)
        logger.addHandler(handler)

        LockBlendsTools()

        # This Instance of the Available class contains all
        # information about packages that are avialable in Debian
        # See below for more information about Available class
        self.data            = ReadConfig(blendname)
        self.blendname       = self.data['Blend']
        self.tasksdir        = FetchTasksFiles(self.data)
        self._InitMetapackages()
        self.tasks           = {}  # Dictionary of TasksDependency objects
        self.alldeps_in_main = []  # sorted string list of package names with all packages
                                   # relevant for a Blend that are in main Debian (for use in DDTP)
        self.alldeps_in_main_info = {}  # complete dictionary with package information
                                   # with all packages relevant for a Blend that are in
                                   # main to easily feed DDTP translation into the structures
                                   # -->
                                   # self.alldeps_in_main = self.alldeps_in_main_info.keys().sort()

    def _InitMetapackages(self):
        # sorted list of metapackage names
        self.metapackagekeys = sorted(
            task for task in os.listdir(self.tasksdir)
            if os.path.isfile(os.path.join(self.tasksdir, task))
        )

    def GetAllDependencies(self, source=False):
        # If we want to subscribe ddpo we need the source package names.
        # In this case set source=True

        # Obtain the prefix of the meta packages of the Blend using blends-dev tools blend_get_names
        if os.access('/usr/share/blends-dev/blend-get-names', os.X_OK):
            blend_get_names = '/usr/share/blends-dev/blend-get-names'
        elif os.access(os.getcwd() + '/blend-get-names', os.X_OK):
            blend_get_names = os.getcwd() + '/blend-get-names'
        else:
            logger.critical("Please either install package Blends-dev or install a copy of devtools/blend-get-names in your working directory")
            exit(-1)

        # The prefix is used to build the meta package name that belongs to the task
        cmd = "cd %s; %s metapackageprefix" % (self.data['datadir'], blend_get_names)
        pipe = Popen(cmd, shell=True, stdout=PIPE).stdout
        prefix = pipe.read().decode('utf-8').strip() + '-'
        pipe.close()

        metapackages = []
        for task in self.metapackagekeys:
            metapackages.append(prefix + task)

        # Verify whether there are any translations of metapackage descriptions which on
        # one hand is a sign that this Blend has uploaded metapackages at all and on the
        # other hand gives a clue about whether it makes sense to query for description
        # translations
        # to verify whether a blend has uploaded metapackage or not we can check the boolean
        # column "metapackage" in blends_tasks table
        query = "SELECT COUNT(*) FROM descriptions WHERE package = ANY ('%s')" % List2PgArray(metapackages)
        _execute_udd_query(query)
        if curs.rowcount > 0:
            hastranslations = curs.fetchone()[0]

        metapkg_translations = {}
        if hastranslations > 0:
            query = "EXECUTE query_metapkg_trans('%s')" % List2PgArray(metapackages)
            _execute_udd_query(query)
            for row in RowDictionaries(curs):
                metapkg_translations[row['package']] = row

        for task in self.metapackagekeys:
            td = TaskDependencies(self.blendname, task=task, tasksdir=self.tasksdir)
            pkgname = prefix + task
            td.SetMetapackageInfo(pkgname, metapkg_translations.get(pkgname))
            logger.debug("Task : %s " % task)
            if td.GetTaskDependencies(source):
                self.tasks[task] = td
            else: # Kick file that is obviously no task file from metapackage list
                self.metapackagekeys = [name for name in self.metapackagekeys if name != task]

        if not source:
            # total number popcon submissions
            query = "EXECUTE popcon_submissions"
            _execute_udd_query(query)
            if curs.rowcount > 0:
                self.popconsubmit = curs.fetchone()[0]
            else:
                self.popconsubmit = None
            # Obtain packages that might enhance any of the packages in tasks list
            self.LinkEnhances()

    def GetNamesOnlyDict(self, dependencystatus=[]):
        # David Paleino needs for his web tools a dictionary
        # { taskname : [list of dependencies]}
        # This will be prepared here from the main
        # datastructure
        ret = {}
        use_dependencystatus = GetDependencies2Use(dependencystatus, 'experimental')

        for task in self.metapackagekeys:
            tdeps = self.tasks[task]
            list = []
            for dep in use_dependencystatus:
                for tdep in tdeps.dependencies[dep]:
                    list.append(tdep.properties['name'])
            ret[task] = list
        return ret

    def GetUpdatablePackages(self, dependencystatus=[]):
        # List of Updatable packages: sourcepkg, version, upstream version
        ret = {}
        use_dependencystatus = GetDependencies2Use(dependencystatus, 'experimental')

        for task in self.metapackagekeys:
            tdeps = self.tasks[task]
            pkgList = []
            for dep in use_dependencystatus:
                for tdep in tdeps.dependencies[dep]:
                    if 'outdated' in tdep.properties:
                        # versions are ordered lists      ---v--- last one is needed
                        pkgList.append({
                            'name': tdep.properties['name'],
                            'debian_version': tdep.properties['version'][-1]['version'],
                            'upstream_version': tdep.properties['outdated']['version'],
                            'maintainer': tdep.properties.get('maintainer'),
                            'uploader': tdep.properties.get('uploader'),
                        })
            if pkgList:
                ret[task] = pkgList
        return ret

    def GetAllDependentPackagesOfBlend(self, dependencystatus=[]):
        # David Paleino needs for his DDTP web tool a list of
        # all available Dependencies.
        # Here only those packages are returned that are in
        # Debian main, because there are no DDTP translations
        # for contrib and non-free available
        if self.alldeps_in_main != []:
            return self.alldeps_in_main

        use_dependencystatus = GetDependencies2Use(dependencystatus, 'unknown')
        for task in self.metapackagekeys:
            tdeps = self.tasks[task]
            for dep in use_dependencystatus:
                for tdep in tdeps.dependencies[dep]:
                    # add only packages in main, because others do not have DDTP translations
                    if tdep.properties['component'] == 'main':
                        self.alldeps_in_main.append(tdep.properties['name'])
                        self.alldeps_in_main_info[tdep.properties['name']] = tdep
            # Also add meta package itself to make use of translated meta package description
            self.alldeps_in_main.append(self.tasks[task].metapkg.properties['name'])
            self.alldeps_in_main_info[self.tasks[task].metapkg.properties['name']] = self.tasks[task].metapkg
        self.alldeps_in_main.sort()
        return self.alldeps_in_main

    def GetTaskDescDict(self):
        # Return dictionary with description information of all tasks of a Blend
        return self.tasks

    def LinkEnhances(self):
        # To provide a connection between packages enhancing other packages a set of links
        # will be provided.  The links should point to paragraphs on the tasks pages if the
        # Enhancing package is listed in the metapackages of the blend and to packages.debian.org
        # otherwise
        for task in self.metapackagekeys:
            tdeps = self.tasks[task]
            for dependency in tdeps.dependencies:
                for dep in tdeps.dependencies[dependency]:
                    if dep.properties['Enhances'] != {}:
                        logger.debug("Package %s is enhanced by:" % dep.properties['name'])
                        for enh in dep.properties['Enhances']:
                            # seek for Enhances on same page
                            found = 0
                            for seek_dependency in tdeps.dependencies:
                                for enhdep in tdeps.dependencies[seek_dependency]:
                                    if enh == enhdep.properties['name']:
                                        dep.properties['Enhances'][enh] = '#' + enh
                                        found = 1  # found enhances in same task
                                        break
                            if found == 0:  # If not found seek in other tasks
                                for enhtask in self.metapackagekeys:
                                    if enhtask == task:
                                        continue
                                    enhtdeps = self.tasks[enhtask]
                                    for seek_dependency in enhtdeps.dependencies:
                                        for enhdep in enhtdeps.dependencies[seek_dependency]:
                                            if enh == enhdep.properties['name']:
                                                dep.properties['Enhances'][enh] = './' + enhtask + '#' + enh
                                                found = 1  # found enhances in other task
                                                break
                                    if found == 1:
                                        break
                            logger.debug(" %s -> %s" % (enh, dep.properties['Enhances'][enh]))

    def getPackageNames(self, sections):
        return set(itertools.chain(*(task.getPackageNames(sections)
                                     for task in self.tasks.values() if task.in_tasklist)))

class TaskDependencies:
    # List of depencencies defined in one metapackage
    def __init__(self, blendname, task, tasksdir=None):

        self.data     = ReadConfig(blendname)
        self.blendname  = self.data['Blend']
        if tasksdir is not None:
            self.tasksdir = tasksdir
        else:
            self.tasksdir = InitTasksFiles(self.data)
        self.taskfile = os.path.join(self.tasksdir, task)
        if os.path.isfile(self.taskfile):
            self.task = task
        else:
            logger.error("No such task file %s." % self.taskfile)
            return None

        # Dictionary with status of dependencies as key and list of DependantPackage
        # instances
        self.dependencies = {}
        for pkgstat in pkgstatus:
            self.dependencies[pkgstat] = []

        # If a Blend just bases on the meta package of an other Blend (this is the
        # case in Debian Science which bases on med-bio for biology and gis-workstation
        # for geography it makes no sense to build an own sentinel page but read
        # meta package information of other meta packages and include the content
        # of these while enabling to add further Dependencies as well
        #
        # metadepends should be a SVN URL
        #
        # This is NOT YET implemented
        self.metadepends     = None

        # Include the task in the task list? Defaults to True.
        self.in_tasklist = True

    def SetMetapackageInfo(self, pkgname, ddtptranslations=None):
        # Gather information (specifically description translations if exists) about metapackage itself
        self.metapkg             = DependantPackage(pkgname)
        self.metapkg.source      = self.blendname
        if not ddtptranslations:
            return
        for lang in languages:
            if ddtptranslations['description_' + lang['ddtp']]:
                self.metapkg.properties['desc'][lang['ddtp']] = {
                    'short': ddtptranslations['description_' + lang['ddtp']]
                }
                try:
                    self.metapkg.properties['desc'][lang['ddtp']]['long'] = PrepareMarkdownInput(ddtptranslations['long_description_' + lang['ddtp']])
                except AttributeError as err:
                    logger.error("===> AttributeError in metapackage long %s (lang='%s'): '%s'; ErrTxt: %s"
                                 % (self.metapkg.properties['name'], lang['ddtp'], ddtptranslations['long_description_' + lang['ddtp']], err))
                    self.metapkg.properties['desc'][lang['ddtp']]['long'] = 'Missing long description'

    def _AppendDependency2List(self, dep, source):
        # Append dependency which was found in the tasks file if not Ignore / Avoid and
        # no dupplication in case of source depencencies
        if dep is None:
            return
        if dep.properties['dep_strength'] in ('Ignore', 'Avoid'):
            logger.debug("Ignore/Avoid package : %s" % dep.properties['name'])
            return
        if not source:
            # In general we can just add the dependency to the list
            self.dependencies[dep.properties['pkgstatus']].append(dep)
            return

        # if we are seeking for ddpo source packages we have to make sure that
        # no duplication occures
        found = False
        for hasdep in self.dependencies[dep.properties['pkgstatus']]:
            if hasdep.properties['name'] == dep.properties['name']:
                found = True
                break
        if not found:
            self.dependencies[dep.properties['pkgstatus']].append(dep)

    def GetTaskDependencies(self, source=False):
        global dep_strength_keys

        found_description = False
        f = codecs.open(self.taskfile, "r", "utf-8")
        for stanza in deb822.Sources.iter_paragraphs(f):
            # Why and Responsible can be valid for more than one dependency
            # Store them in strings and use them for all Dependent Package objects
            why               = None
            responsible       = None
            dep               = None
            remark            = {}
            tmp_dep_list      = []
            fields_duplicated = None
            fields_obsolete   = []
            for key in stanza:
                if key == 'Task':
                    # also the task name might be utf-8 encoded
                    self.metapkg.properties['PrintedName'] = stanza['task']
                    continue
                if key == 'Description':
                    if found_description:
                        logger.error("Duplicate description entry in task %s; you probably want to use Pkg-Description field instead!" % self.task)
                    else:
                        (shortDesc, longDesc) = SplitDescription(stanza['description'])
                        self.metapkg.properties['desc']['en']['short'] = shortDesc
                        self.metapkg.properties['desc']['en']['long']  = longDesc
                        found_description = True
                    continue
                if key == 'Index':
                    self.in_tasklist = (stanza['index'].lower() != 'false')
                    continue
                if key == 'Meta-Depends':
                    self.metadepends = stanza['meta-depends']
                    continue
                if key == 'Meta-Suggests':
                    self.metadepends = stanza['meta-suggests']
                    continue
                if key == 'Why':
                    why = stanza['why']
                    continue
                if key == 'Responsible':
                    responsible = stanza['responsible'].strip()
                    if not dep:
                        # Make sure there is really enough information to deal with provided by the package
                        logger.error("Missing package information for field %s = %s in task file %s" % (key, responsible, self.task))
                        continue
                    if 'maintainer' in dep.properties:
                        # we are dealing with an official package that has a real maintainer who
                        # is finally responsible
                        # ... but do not issue a hint about this in the logs. Sometimes a responsible
                        # person makes sense in the tasks field
                        # fields_obsolete.append(key)
                        continue
                    if responsible:
                        (_name, _url) = email.utils.parseaddr(responsible)
                        dep.properties['maintainer'] = {'name':_name, 'email': _url}
                    continue

                if key in dep_strength_keys:
                    # Hack: Debian Edu tasks files are using '\' at EOL which is broken
                    #       in RFC 822 files, but blend-gen-control from blends-dev relies
                    #       on this.  So remove this stuff here for the Moment
                    dependencies = re.sub('\\\\\n\s+', '', stanza[key])

                    # Remove versions from versioned depends
                    dependencies = re.sub(' *\([ ><=\.0-9]+\) *', '', dependencies)

                    for dep_in_line in dependencies.split(','):
                        dep_in_line = dep_in_line.strip()
                        if not dep_in_line:
                            continue

                        # Each dependency may have alternatives delimited by "|".
                        for i, dep_name in enumerate(dep_in_line.split('|')):
                            dep_name = dep_name.strip()
                            if not dep_name:
                                continue

                            if not dep_in_line.islower():
                                logger.warning("Package names may not contain upper case letters, so %s is an invalid package name which is turned into %s"
                                               % (dep_name, dep_name.lower()))
                                dep_name = dep_name.lower()
                            dep = DependantPackage(dep_name)
                            # Store the comments in case they might be usefull for later applications
                            if why:
                                dep.properties['why'] = why
                            if responsible:
                                (_name, _url) = email.utils.parseaddr(responsible)
                                dep.properties['maintainer'] = {'name':_name, 'email': _url}
                            if i > 0 and key == "Depends":
                                # Alternatives will be lowered to suggestions.
                                dep.properties['dep_strength'] = "Suggests"
                            else:
                                dep.properties['dep_strength'] = key
                            tmp_dep_list.append(dep)

                # sometimes the tasks file contains standalone comments or other RFC 822 entries.
                # Just ignore this stuff
                if dep is None:
                    continue  # ... with next stanza

                # the following fields might be provided in the Blends tasks file for
                # prospective packages.  This information should be ignored in case the
                # package just exists at Debian mirrors
                if pkgstatus[dep.properties['pkgstatus']]['order'] <= pkgstatus['experimental']['order']:
                    # for packages not in Debian we use the information from the tasks file
                    # if a package is in Debian we use the information from the Packages file (via UDD query)
                    # and the fields may not be overriden.  The list collects duplicated fields and will
                    # trigger a warning if the list is not empty
                    # TODO: warn about possibly duplicated prospective package entries in tasks files
                    fields_duplicated = []

                # The following keys will be mostly used for programs that
                # are not yet existing in Debian and will go to our todo list
                if key == 'Homepage':
                    if fields_duplicated is not None:
                        fields_duplicated.append(key)
                    # set Homepage only if not just set via official package information
                    if dep.properties['homepage'] == HOMEPAGENONE:
                        dep.properties['homepage'] = stanza['homepage']
                    else:
                        fields_obsolete.append(key)
                elif key.lower() in ('vcs-svn', 'vcs-git'):
                    vcs = dep.properties.setdefault('vcs', {})
                    vcs['url']  = stanza[key.lower()]
                    vcs['type'] = key.split('-')[1].capitalize()
                    if 'browser' not in vcs:
                        try:
                            vcs['browser'] = BrowserFromVcsURL(vcs['type'], vcs['url'])
                        except KeyError as err:
                            logger.error("Vcs Property missing in packages file:", vcs, err)
                    if dep.properties['pkgstatus'] == 'unknown':
                        dep.properties['pkgstatus'] = 'pkgvcs'
                elif key.lower() == 'vcs-browser':
                    vcs = dep.properties.setdefault('vcs', {})
                    vcs['browser'] = stanza['vcs-browser']
                    if re.compile("[/.]git\.").search(vcs['browser']):
                        vcs['type'] = 'Git'
                    elif re.compile("[/.]svn\.").search(vcs['browser']):
                        vcs['type'] = 'Svn'
                    else:
                        # no chance to guess Vcs type
                        vcs['type'] = 'Vcs'
                    # There is no need to specify the Vcs-{Git,SVN} field in the tasks file but property 'vcs-type' should be set in
                    # any case - so set it here in case it was not set before.  If an apropriate field is set later it becomes
                    # overriden anyway
                    if 'url' not in vcs:
                        vcs['url'] = vcs['browser']
                    if dep.properties['pkgstatus'] == 'unknown':
                        dep.properties['pkgstatus'] = 'pkgvcs'
                elif key == 'section':
                    dep.properties[key.lower()]  = stanza[key.lower()]
                elif key == 'License':
                    dep.properties[key.lower()]  = stanza[key.lower()]
                elif key == 'Language':
                    dep.properties[key.lower()]  = stanza[key.lower()]
                elif key == 'Registration':
                    dep.properties[key.lower()]  = stanza[key.lower()]
                elif key.startswith('Published-'):
                    if 'published' not in dep.properties:
                        dep.properties['published'] = {}
                    ptype = key.replace('Published-', '').lower()
                    dep.properties['published'][ptype] = stanza[key.lower()]
                elif key == 'WNPP':
                    wnpp = stanza['wnpp'].strip()
                    # in case somebody prepended a '#' sign before the bug number
                    wnpp = re.sub('^#', '', wnpp)
                    # if there is really a number given
                    if re.compile("^\d+$").search(wnpp):
                        dep.properties['wnpp'] = wnpp
                elif key.lower() == 'pkg-url':
                    if dep.properties['pkg-url'] == HOMEPAGENONE:  # only if no official package is just available
                        dep.properties['pkg-url'] = stanza['pkg-url']
                elif key == 'Pkg-Description':
                    # Only update use description from task file if not known from official package
                    if dep.properties['desc']['en'] == {}:
                        (shortDesc, longDesc) = SplitDescription(stanza['pkg-description'])
                        dep.properties['desc']['en']['short'] = shortDesc
                        dep.properties['desc']['en']['long']  = longDesc
                    else:
                        fields_obsolete.append(key)
                elif key == 'Avoid' or key == 'Ignore':
                    dep.properties['pkgstatus'] = key.lower()
                elif key == 'Remark':
                    (shortDesc, longDesc) = SplitDescription(stanza['remark'])
                    if shortDesc:
                        remark['short'] = shortDesc
                    if longDesc:
                        remark['long'] = longDesc
                else:
                    if key not in KEYSTOIGNORE and key not in KEYSVALID:
                        # Also ignore keys starting with X[A-Z]-
                        if not re.compile("^X[A-Z]*-").search(key):
                            try:
                                logger.warning("Unknown key '%s': %s in file %s" % (key, stanza[key], self.metapkg.properties['PrintedName']))
                            except:
                                logger.error("Unknown key '%s' with problematic value in file %s." % (key, self.metapkg.properties['PrintedName']))

            if dep is None:
                continue  # ... with next stanza
            # seek for certain field set in the tasks file to move the dependency into different
            # categories of development status of not yet included packages provided that there
            # is at least a package description given
            if dep.properties['pkgstatus'] == 'unknown' and dep.properties['desc']['en'] != {}:
                flag = 0
                # first check those packages where some work was just done
                for status in ['pkgvcs', 'unofficial', 'wnpp', 'prospective']:
                    for field in pkgstatus[status]['fields-set']:
                        if field in dep.properties:
                            if field in HOMEPAGENONEFIELDS and dep.properties[field] == HOMEPAGENONE :
                                continue
                            dep.properties['pkgstatus'] = status
                            flag = 1
                            break
                    if flag == 1:
                        break

            # remarks which are common to several dependencies in a list have
            # to be added to all of the dependencies
            for dep in tmp_dep_list:
                if remark:
                    dep.properties['remark'] = remark
                if fields_obsolete != [] and dep.properties['pkgstatus'] != 'new':
                    logger.info("Package %s is an official package and has information in UDD. The following extra information can be removed from tasks file %s: %s"
                                % (dep.properties['name'], self.task, str(fields_obsolete)))
                self._AppendDependency2List(dep, source)

        f.close()

        alldepends = []
        for status in self.dependencies.keys():
            for dep in self.dependencies[status]:
                alldepends.append(dep.properties['name'])

        if 'PrintedName' not in self.metapkg.properties:
            logger.error("Task file %s is lacking field 'Task' - and thus will be ignored" % self.taskfile)
            return 0  # Failure
        if not alldepends:
            logger.warning("No dependencies defined in taskfile %s" % self.task)
            return 0  # Failure
        query = "EXECUTE query_pkgs ('%s', '%s', '%s')" % (List2PgArray(alldepends), List2PgSimilarArray(alldepends), self.task)
        _execute_udd_query(query)
        pkgs_in_pool = []
        enhancing_pkgs = []
        if curs.rowcount > 0:
            for row in RowDictionaries(curs):
                # seek for package name in list of packages mentioned in tasks file
                found = False
                for status in self.dependencies.keys():
                    for dep in self.dependencies[status]:
                        if dep.properties['name'] == row['package']:
                            found = True
                            break
                    if found:
                        break
                if not found:
                    # this should not happen ...
                    logger.info("The package %s was found in package pool but never mentioned in task %s." % (row['package'], self.task))
                    continue

                # Now set the information for the package found in the database
                # Debian Edu contains packages from main/debian-installer - that's why we use startswith here
                if row['component'].startswith('main'):
                    dep.properties['component'] = 'main'
                    if dep.properties['dep_strength'] in ('Depends', 'Recommends'):
                        dep.properties['pkgstatus'] = 'official_high'
                    elif dep.properties['dep_strength'] == 'Suggests':
                        dep.properties['pkgstatus'] = 'official_low'
                else:
                    dep.properties['component'] = row['component']
                    # If a package is not found in main its status can be maximum non-free
                    dep.properties['pkgstatus'] = 'non-free'
                # if a package is released *only* in experimental decrease package status
                if row['release'] == 'experimental':
                    dep.properties['pkgstatus'] = 'experimental'

                # move dependant package to different status list if needed because a status change was detected
                if dep.properties['pkgstatus'] != status:
                    self.dependencies[status].remove(dep)
                    self.dependencies[dep.properties['pkgstatus']].append(dep)

                # Warn about remaining information of prospective package
                if dep.properties['desc']['en'] and dep.properties['desc']['en']['short'] and 'debtags' not in dep.properties:
                    # prevent informing about packages which are just duplicated because of a broken query
                    logger.info("WNPP for package %s just closed - extra information can be removed from task file %s."
                                % (dep.properties['name'], self.task))

                dep.properties['license'] = license_in_component[dep.properties['component']]
                for prop in PROPERTIES:
                    dep.properties[prop] = row[prop]
                for prop in ('upstream_bugs', 'upstream_repository'):
                    dep.properties[prop] = row[prop]
                for prop in ('type', 'url', 'browser'):
                    if row['vcs-'+prop]:
                        vcs = dep.properties.setdefault('vcs', {})
                        vcs[prop] = row['vcs-'+prop]
                if 'vcs' in dep.properties:
                    vcs = dep.properties['vcs']
                    if 'browser' not in dep.properties['vcs']:
                        vcs['browser'] = BrowserFromVcsURL(vcs['type'], vcs['url'])

                if row.get('enhanced'):
                    for pkg in row['enhanced']:
                        dep.properties['Enhances'][pkg] = PKGURLMASK % pkg
                        enhancing_pkgs.append(pkg)

                if 'releases' in row:
                    for rel, ver, arch in zip(row['releases'], row['versions'],
                                              row['architectures']):
                        dep.properties['version'].append({
                            'url': 'https://packages.debian.org/{0}/{1}'.format(rel, dep.properties['name']),
                            'release': rel,
                            'version': ver,
                            'archs': arch
                        })

                if 'vote' in row:
                    dep.properties.setdefault('popcon', {})['vote'] = row['vote']
                if 'recent' in row:   
                    dep.properties.setdefault('popcon', {})['recent'] = row['recent']
                if 'insts' in row:
                    dep.properties.setdefault('popcon', {})['insts'] = row['insts']

                # Debtags
                if row.get('debtags'):
                    debtags = dep.properties.setdefault('debtags', {})
                    # there is no reasonable way that debtags was set before,
                    # so something is wrong here and a warning should be issued 
                    if debtags:
                        logger.warning("Debtags for package '%s' was just set." % dep.properties['name']
                                       + " A duplicated result from database query is suspected."
                                       + " Please check the result!")

                    for debtag in row['debtags']:
                        (tag, value) = debtag.split('::')
                        debtags.setdefault(tag, []).append(value)

                # screenshots
                if row.get('icon'):
                    img = dep.properties.setdefault('screenshot', {})
                    img['icon'] = row['icon'][0]
                    img['image'] = row['image'][0]
                    if len(row['image']) > 1:
                        screenshots = img.setdefault('screenshots', [])
                        for i in range(1, len(row['image'])):
                            screenshots.append({
                                'version': row['screenshot_versions'][i],
                                'url': row['image'][i]
                            })

                # it might be that the new upstream goes to experimental - this should be ignored here
                if row.get('unstable_parsed_version'):
                    dep.properties['outdated'] = {
                        'release': 'upstream',
                        'version': row['unstable_upstream'],
                        'architectures': 'all',
                    }

                if row['changed_by']:
                    changed = row['changed_by']
                    if changed:
                        (_name, _url) = email.utils.parseaddr(changed)
                        dep.properties['uploader'] = {'name': _name, 'email': _url}

                # link to packages.debian.org search page to see overview about all
                # package versions in all releases
                dep.properties['pkg-url'] = PKGURLMASK % dep.properties['name']

                for lang in languages:
                    if row.get('description_' + lang['ddtp']):
                        dep.properties['desc'][lang['ddtp']] = {
                            'short': row['description_' + lang['ddtp']]
                        }
                        if row['long_description_' + lang['ddtp']]:
                            dep.properties['desc'][lang['ddtp']]['long'] = PrepareMarkdownInput(row['long_description_' + lang['ddtp']])
                if 'short' not in dep.properties['desc']['en']:
                    logger.error("Dep has no English short description: %s", dep.properties['name'])
                    dep.properties['desc']['en']['short'] = "??? missing short description for package %s :-(" % dep.properties['name']

                if row['maintainer']:
                    (_name, _url) = email.utils.parseaddr(row['maintainer'])
                    dep.properties['maintainer'] = {'name': _name, 'email': _url}

                for prop in ('biotools', 'omictools', 'bioconda', 'scicrunch', 'rrid', 'bugs', 'edam_topics'):
                    if prop in row:
                        if row[prop] != None and row[prop] != 'NA':
                            if prop == 'edam_topics':
                                dep.properties[prop] = "; ".join(row[prop])
                                #logger.info("Property edam_topics for package %s is set to %s" % (dep.properties['name'], dep.properties[prop]))
                            else:
                                dep.properties[prop] = row[prop]

                # Publications
                setPublications(dep, self.task, row)

                pkgs_in_pool.append(dep.properties['name'])
                # DEBUG
                # print dep

        pkgs_not_in_pool = []
        for status in self.dependencies.keys():
            for dep in self.dependencies[status]:
                if dep.properties['name'] not in pkgs_in_pool:
                    pkgs_not_in_pool.append(dep.properties['name'])

        # Gather information about packages in NEW
        query = "EXECUTE query_new('%s')" % List2PgArray(pkgs_not_in_pool)
        _execute_udd_query(query)
        pkgs_in_new = []
        if curs.rowcount > 0:
            for row in RowDictionaries(curs):
                pkgs_in_new.append(row['package'])
                # seek for package name in list of packages mentioned in tasks file
                found = False
                for status in self.dependencies.keys():
                    for dep in self.dependencies[status]:
                        if dep.properties['name'] == row['package']:
                            found = True
                            break
                    if found:
                        break
                if not found:
                    # this should not happen ...
                    logger.info("The package %s was found in new but never mentioned in task %s." % (row['package'], self.task))
                    continue
                # Check for korrekt status 'new'
                if status != 'new':
                    self.dependencies[status].remove(dep)
                    self.dependencies['new'].append(dep)
                dep.properties['pkgstatus'] = 'new'
                dep.properties['component'] = row['component']
                dep.properties['version'].append({'archs':'all', 'release':'NEW',
                                                  'version':row['version']})
                dep.properties['pkg-url'] = 'https://ftp-master.debian.org/new/%s_%s.html' % (row['source'], row['version'])
                for prop in ('url', 'browser', 'type'):
                    if row['vcs-' + prop]:
                        vcs = dep.properties.setdefault('vcs', {})
                        vcs[prop] = row['vcs-' + prop]
                # Warn about remaining information of prospective package
                if (dep.properties['desc']['en'] and dep.properties['desc']['en']['short']) or dep.properties['homepage'] != HOMEPAGENONE:
                    logger.info("The package %s is not yet in Debian but it is just in the new queue. (Task %s)" % (dep.properties['name'], self.task))
                for prop in PROPERTIES:
                    dep.properties[prop] = row[prop]
                dep.properties['desc']['en']['short'] = row['description_en']
                dep.properties['desc']['en']['long']  = PrepareMarkdownInput(row['long_description_en'])
                if row['maintainer']:
                    (_name, _url) = email.utils.parseaddr(row['maintainer'])
                    dep.properties['maintainer'] = {'name': _name, 'email': _url}

                setPublications(dep, self.task, row)
                if row['changed_by']:
                    try:
                        changed = row['changed_by']
                    except TypeError as err:
                        changed = None
                        logger.warning("Encoding problem for uploader to ftpnew of package '%s' in task %s (%s)" % (dep.properties['name'], self.task, err))
                    if changed:
                        (_name, _url) = email.utils.parseaddr(changed)
                        dep.properties['uploader'] = {'name': _name, 'email': _url}

        # Verify whether there are virtual packages which are provided by some other packages in the list of dependencies
        query = "EXECUTE query_provides('%s')" % List2PgArray(pkgs_in_pool)
        _execute_udd_query(query)
        pkgs_virtual = []
        if curs.rowcount > 0:
            virtual_pkgs = RowDictionaries(curs)
            for status in self.dependencies.keys():
                for dep in self.dependencies[status]:
                    if dep.properties['name'] not in pkgs_in_pool and dep.properties['name'] not in pkgs_in_new:
                        found = False
                        for vp in virtual_pkgs:
                            for pr in vp['provides'].split(','):
                                prs = pr.strip()
                                if dep.properties['name'] == prs:
                                    pkgs_virtual.append(prs)
                                    logger.info("Virtual package %s is provided by package %s for task %s" % (dep.properties['name'], vp['package'], self.task))
                                    found = True
                                    break
                            if found:
                                break

        pkgs_not_in_pool = []
        for status in self.dependencies.keys():
            for dep in self.dependencies[status]:
                if dep.properties['name'] not in pkgs_in_pool and dep.properties['name'] not in pkgs_virtual:
                    pkgs_not_in_pool.append(dep.properties['name'])
        # Gather information about packages in Vcs
        query = "EXECUTE query_vcs('%s')" % List2PgArray(pkgs_not_in_pool)
        _execute_udd_query(query)
        pkgs_in_vcs = []
        if curs.rowcount > 0:
            for row in RowDictionaries(curs):
                # print row
                pkgs_in_vcs.append(row['package'])
                # seek for package name in list of packages mentioned in tasks file
                found = False
                for status in self.dependencies.keys():
                    for dep in self.dependencies[status]:
                        if dep.properties['name'] == row['package']:
                            found = True
                            break
                    if found:
                        break
                if not found:
                    # this should not happen ...
                    logger.info("The package %s was found in vcs but never mentioned in task %s." % (row['package'], self.task))
                    continue
                # Check for korrekt status 'pkgvcs'
                if status not in ('pkgvcs', 'new'):
                    self.dependencies[status].remove(dep)
                    self.dependencies['pkgvcs'].append(dep)
                    status = 'pkgvcs'
                dep.properties['pkgstatus'] = status
                dep.properties['component'] = row['component']
                dep.properties['version'].append({'archs':'all', 'release':'VCS',
                                                  'version':row['version']})
                # Warn about remaining information of prospective package
                if (dep.properties['desc']['en'] and dep.properties['desc']['en']['short']) or dep.properties['homepage'] != HOMEPAGENONE:
                    logger.info("The package %s is not yet in Debian but it is just in Blends %s Vcs. (Task %s)" % (dep.properties['name'], row['blend'], self.task))
                for prop in PROPERTIES:
                    dep.properties[prop] = row[prop]
                dep.properties['license'] = row['license']
                for prop in ('url', 'browser', 'type'):
                    if row['vcs-' + prop]:
                        vcs = dep.properties.setdefault('vcs', {})
                        vcs[prop] = row['vcs-' + prop]
                if int(row['wnpp']) > 0:
                    dep.properties['wnpp'] = row['wnpp']
                setPublications(dep, self.task, row)
                dep.properties['desc']['en']['short'] = row['description_en']
                dep.properties['desc']['en']['long'] = PrepareMarkdownInput(row['long_description_en'])
                if row['maintainer']:
                    (_name, _url) = email.utils.parseaddr(row['maintainer'])
                    dep.properties['maintainer'] = {'name': _name, 'email': _url}

                if row['changed_by']:
                    try:
                        changed = row['changed_by']
                    except TypeError as err:
                        changed = None
                        logger.warning("Encoding problem for changelog author in Vcs of package '%s' in task %s (%s)" % (dep.properties['name'], self.task, err))
                    if changed:
                        (_name, _url) = email.utils.parseaddr(changed)
                        dep.properties['uploader'] = {'name': _name, 'email': _url}

        # Verify whether packages which are neither in pool, new, vcs nor virtual have sufficient information in task file
        for status in self.dependencies.keys():
            for dep in self.dependencies[status]:
                if dep.properties['name'] not in pkgs_in_pool and dep.properties['name'] not in pkgs_in_new and dep.properties['name'] not in pkgs_virtual and dep.properties['pkgstatus'] in ('unknown', 'pkgvcs'):
                    # If only Vcs fields are given than we currently do not know enough to print package information
                    if dep.properties['pkgstatus'] == 'pkgvcs' and (dep.properties['homepage'] == HOMEPAGENONE or dep.properties['desc']['en'] == {}):
                        logger.error("Package %s in task %s has only Vcs information - please provide more information" % (dep.properties['name'], self.task))
                        self.dependencies[status].remove(dep)
                        # dep.properties['pkgstatus'] == 'unknown'
                    if dep.properties['homepage'] == HOMEPAGENONE:
                        if dep.properties['desc']['en'] == {}:
                            logger.error("Package %s in task %s neither in pool nor new and is lacking homepage and description - ignored" % (dep.properties['name'], self.task))
                        else:
                            logger.error("Package %s in task %s neither in pool nor new and has no homepage information - ignored (%s)" % (dep.properties['name'], self.task, dep.properties['pkgstatus']))
                    else:
                        if dep.properties['desc']['en'] == {}:
                            logger.error("Package %s neither in pool nor new and has no description - ignored" % dep.properties['name'])
                else:
                    # prevent printing WNPP of packages inside Debian
                    if 'wnpp' in dep.properties and dep.properties['pkgstatus'] not in ('wnpp', 'pkgvcs'):
                        del dep.properties['wnpp']

        for dependency in self.dependencies.keys():
            self.dependencies[dependency].sort(key = lambda x: x.properties['name'])
        return 1  # Success

    def getPackageNames(self, sections):
        return set(itertools.chain(*(
            (dep.properties['name'] for dep in self.dependencies[d])
            for d in sections
        )))

    def _QueryUDD4Package(self, source):
        query = "EXECUTE pkg_releases ('%s', '%s')" % (self.properties['name'], self.properties['component'])
        _execute_udd_query(query)
        has_official = 0
        for rel in curs.fetchall():
            self.properties.setdefault('releases', []).append(rel[0])
            if rel[0] != 'experimental':
                has_official = 1

        # Version in stable / testing for long table
        query = "EXECUTE pkg_versions_stable_testing ('%s')" % (self.properties['name'])
        _execute_udd_query(query)
        if curs.rowcount > 0:
            prefix = 'Versions: '
            for row in RowDictionaries(curs):
                self.properties['stable_testing_version'].append((row['release'], row['debversion'], row['version'], prefix))
                prefix = ', '

        if has_official == 1:
            if self.properties['component'] == 'main':
                if self.properties['dep_strength'] in ('Depends', 'Recommends'):
                    self.properties['pkgstatus'] = 'official_high'
                else:
                    self.properties['pkgstatus'] = 'official_low'
            else:
                    self.properties['pkgstatus'] = 'non-free'
        else:
            self.properties['pkgstatus'] = 'experimental'

        if 'source' in self.properties:
            query = "EXECUTE src_vcs ('%s')" % (self.properties['source'])
            _execute_udd_query(query)
            if curs.rowcount > 0:
                # There is only one line returned by this query
                row = RowDictionaries(curs)[0]
                # If some information about Vcs is found in the database make sure it is ignored from tasks file
                for prop in row.keys():
                    if row[prop]:
                        self.properties[prop] = row[prop]
                if 'vcs' in self.properties:
                    vcs = self.properties['vcs']
                    if 'type' not in vcs:
                        vcs['type'] = VcsTypeFromBrowserURL(vcs.get('browser', vcs['url']))
                    if 'browser' not in self.properties['vcs']:
                        vcs['browser'] = BrowserFromVcsURL(vcs['type'], vcs['url'])
            # We are only interested in source packages (for instance for Bugs page)
            if source:
                self.properties['name'] = self.properties['source']
            # Stop using source package in self.properties['name'] because we need the source package to obtain latest uploaders and
            # and bugs should be rendered in the same job - so we need the differentiation anyway
            self.src = self.properties['source']
        else:
            logger.error("Failed to obtain source for package", self.properties['name'])
            return

        if not source:
            # If we are querying for source packages to render BTS pages
            # tranlations are irrelevant - so only obtain ddtp translations
            # otherwise
            ######################
            if flag == 0:
                # If there was no such package found query UDD whether any package provides this name
                # This is often the case for libraries with versions in the package name
                query = "EXECUTE query_provides ('%s')" % (dep.properties['name'])
                _execute_udd_query(query)
                if curs.rowcount > 0:
                    has_expilicite = 0
                    VirtProvides = []
                    for row in curs.fetchall():
                        VirtProvides.append(row[0])
                        for hasdeps in tmp_dep_list:
                            if row[0] == hasdeps.properties['name']:
                                logger.error("    --> %s is mentioned explicitely in dependency list" % row[0])
                                has_expilicite = 1
                    if has_expilicite == 1:
                        logger.error("Do not keep a record of virtual package %s which has explicite package dependencies" % dep.properties['name'])
                        # ATTENTION: THIS HAS TO BE CHANGED FOR blends-dev BY AN OPTIONAL parameter
#                        continue

                    logger.error("Use real package %s instead of virtual package %s." % (VirtProvides[0], dep.properties['name']))
                    dep.properties['name'] = VirtProvides[0]
                    dep._QueryUDD4Package(source)

                    if len(VirtProvides) > 1:
                        logger.error("Virtual package %s is provided by more than one package (%s).  Make sure you mention a real package in addition!"
                                     % (dep.properties['name'], str(VirtProvides)))

                        for virt_provides in VirtProvides[1:]:
                            # Add all remaining packages which provide a virtual package to the list
                            if dep is not None:
                                # Add the first real package from the packages which provide the virtual package to the list
                                tmp_dep_list.append(dep)
                            dep = DependantPackage(virt_provides)
                            # Store the comments in case they might be usefull for later applications
                            if why:
                                dep.properties['why'] = why
                            if responsible:
                                (_name, _url) = email.utils.parseaddr(responsible)
                                dep.properties['maintainer'] = {'name':_name, 'email': _url}
                            dep.properties['dep_strength'] = key
                            dep._QueryUDD4Package(source)
                else:
                    logger.warning("Dependency with unknown status: %s (Task %s)" % (dep.properties['name'], self.task))

        for dependency in self.dependencies.values():
            dependency.sort(key = lambda x: x.properties['name'])

    def __str__(self):
        ret = "Blend: " + self.blendname + ", " \
              "Task:"   + self.task      + ", " \
              "Dependencies:" + str(self.dependencies)
        return ret


class Available:
    # Information about available packages
    #
    # Usage example:
    #    available = Available(                     # Initialize instance
    #                          release='testing',      # (default='unstable')
    #                          components=('main'), # Regard only main, default: main, contrib, non-free
    #                          source=True,         # Use source package names, default: use binaries
    #                          arch='sparc'         # (default='i386')
    #                         )
    #
    #    available.GetPackageNames() # Actually parse the Packages files to obtain needed information
    #                                # This has to be done at least once.  It is verified that the effort
    #                                # to obtain package information is not done twice per run

    def __init__(self, release=None, components=(), source=None, arch=None, method=None):
        if method == 'UDD':
            self.__init_UDD__(release=release, components=components, source=source, arch=arch)
        else:
            self.__init_Packages_gz__(release=release, components=components, source=source, arch=arch)

    def __init_Packages_gz__(self, release=None, components=(), source=None, arch=None):
        self.source = 'Packages.' + COMPRESSIONEXTENSION
        if source is not None:
            self.source = 'Sources.' + COMPRESSIONEXTENSION
        self.binary = 'source'
        if source is None:
            if arch is None:
                # use arch=i386 as default because it contains most packages
                self.binary = 'binary-i386'
            else:
                self.binary = 'binary-' + arch
        self.release = 'unstable'
        if release is not None:
            self.release = release
        self.components = ('main', 'contrib', 'non-free')
        if components != ():
            self.components = components
        # The dictionary packages contains the component as key
        # The values are dictionaries holding package names as key
        # and a DependantPackage object as values
        self.packages = {}
        for component in self.components:
            self.packages[component] = {}


##################################################################################################
# bugs

SEVERITIES = ('critical', 'grave', 'serious', 'important', 'normal', 'minor', 'wishlist')

# Obtain description in foreign language from DDTP project if available
# For the moment just query for the highest available version of the description
query = """PREPARE bugs_query_source (text) AS
           SELECT id, package, source, status, severity, done, title FROM bugs WHERE source = $1"""
_execute_udd_query(query)

query = """PREPARE bugs_query_tags (int) AS SELECT tag FROM bugs_tags WHERE id = $1"""
_execute_udd_query(query)


class BugEntry:
    # Define a separate class for bug entries to be able to define a reasonably sorting mechanism
    # It seems that Genshi templates are unable to handle bug objects and so the essential parts
    # are taken over here in addition to a __cmp__ method to enable easy sorting of list of bugs
    # Rationale: Calling methods inside the template does not
    #            seem to work and has to be done in any case to
    #            circumvent encoding problems

    def __init__(self, bug):
        self.bug      = bug['id']
        self.summary  = bug['title']
        self.severity = bug['severity']

        query = "EXECUTE bugs_query_tags (%i)" % self.bug
        _execute_udd_query(query)

        self.tags     = ''
        if curs.rowcount > 0:
            komma = ''
            for tag in curs.fetchall():
                self.tags += komma + tag[0]
                komma = ', '

    # sort these objects according to bug number
    def __cmp__(self, other):
        # Comparing with None object has to return something reasonable
        if other is None:
            return -2
        # Sort according to package name
        return cmp(self.bug, other.bug)


class PackageBugs:
    # Store list of bugs (either open or done) of a package

    def __init__(self, pkgdict):
        self.pkgname    = pkgdict['pkgname']
        self.source     = pkgdict['source']
        self.homepage   = pkgdict['homepage']
        self.vcsbrowser = pkgdict['vcs']['browser']
        self.bugs       = []      # open bugs
        self.nbugs      = 0
        self.severities = {}
        for s in SEVERITIES:
                self.severities[s] = 0

    # sort these objects according to the package name
    def __cmp__(self, other):
        # Comparing with None object has to return something reasonable
        if other is None:
            return -2
        # Sort according to package name
        return cmp(self.pkgname, other.pkgname)

    def __str__(self):
        str = "---\npkgname: %s\nsource: %s\n" % (self.pkgname, self.source)
        if self.homepage:
            str += "homepage: %s\n" % (self.homepage)
        if self.vcsbrowser:
            str += "vcsbrowser: %s\n" % (self.vcsbrowser)
        return str + "nbugs: %s\nnbugs = %i\nseverities = %s\n---" % (self.nbugs, self.severities)


class PackageBugsOpenAndDone:
    # Store list of bugs of a package

    def __init__(self, pkgdict):
        pkgname = pkgdict['pkgname']
        source  = pkgdict['source']
        self.open       = PackageBugs(pkgdict)  # open bugs
        self.done       = PackageBugs(pkgdict)  # closed bugs

        bugs = None
        query = "EXECUTE bugs_query_source ('%s')" % source
        _execute_udd_query(query)

        if curs.rowcount > 0:
            for bug in RowDictionaries(curs):
                bugentry = BugEntry(bug)
                if bug['status'] == 'done':
                    if self.done.pkgname is None:
                        self.done.pkgname = pkgname
                    self.done.bugs.append(bugentry)
                    self.done.nbugs += 1
                else:
                    if self.open.pkgname is None:
                        self.open.pkgname = pkgname
                    self.open.bugs.append(bugentry)
                    self.open.nbugs += 1
                    self.open.severities[bugentry.severity] += 1
            self.open.bugs.sort()
            self.done.bugs.sort()
            if source is None:
                self.open.source = pkgname
                self.done.source = pkgname
            else:
                self.open.source = source
                self.done.source = source
