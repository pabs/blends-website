#!/bin/sh
NAME=blends-webtools

xgettext \
	--default-domain="$NAME" \
	--package-name="$NAME" \
	--package-version="0.1.1" \
	--msgid-bugs-address="debian-custom@lists.debian.org" \
	--copyright-holder="Debian Pure Blends Team <debian-custom@lists.debian.org>" \
	--keyword="_" \
	--output-dir="po" \
	--escape \
	--output="${NAME}.pot" \
	--language="Python" \
	tasks.py templates/tasks_idx.xhtml templates/tasks.xhtml \
	bugs.py templates/bugs_idx.xhtml templates/bugs.xhtml

# Fake xgettext output for config files
grep --with-filename --line-number "_(.\+)" webconf/*.conf | \
	perl -ne '/^([^:]+):([0-9]+):.*_\(\W(.+)\W\)/; print "\n#: $1:$2\nmsgid \"$3\"\nmsgstr\"\"\n";' \
        >> po/${NAME}.pot

exit
