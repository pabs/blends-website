#!/usr/bin/python
# Copyright 2013: Andreas Tille <tille@debian.org>
# License: GPL

#PORT=5441
UDDPORT=5452
PORT=UDDPORT
DEFAULTPORT=5432

from sys import stderr, exit
import psycopg2
import json
import re

###########################################################################################
# Define several prepared statements to query UDD
try:
  conn = psycopg2.connect(host="localhost",port=PORT,user="guest",database="udd")
except psycopg2.OperationalError, err:
  try:
    conn = psycopg2.connect(host="udd.debian.org",port=UDDPORT,user="guest",database="udd")
  except psycopg2.OperationalError, err:
    # logger not known at this state: logger.warning
    print >>stderr, "PostgreSQL does not seem to run on port %i .. trying default port %i.\n\tMessage: %s" % (PORT, DEFAULTPORT, str(err))
    try:
        conn = psycopg2.connect(host="localhost",port=DEFAULTPORT,user="guest",database="udd")
    except psycopg2.OperationalError:
	# Hmmm, I observed a really strange behaviour on one of my machines where connecting to
	# localhost does not work but 127.0.0.1 works fine.  No idea why ... but this should
	# do the trick for the moment
	conn = psycopg2.connect(host="127.0.0.1",port=DEFAULTPORT,user="guest",database="udd")

curs = conn.cursor()
# uddlog = open('logs/uddquery.log', 'w')

def _execute_udd_query(query):
    try:
        curs.execute(query)
    except psycopg2.ProgrammingError, err:
        print >>stderr, "Problem with query\n%s" % (query)
        print >>stderr, err
        exit(-1)
    except psycopg2.DataError, err:
        print >>stderr, "%s; query was\n%s" % (err, query)

def RowDictionaries(cursor):
    """Return a list of dictionaries which specify the values by their column names"""

    description = cursor.description
    if not description:
        # even if there are no data sets to return the description should contain the table structure.  If not something went
        # wrong and we return NULL as to represent a problem
        return NULL
    if cursor.rowcount <= 0:
        # if there are no rows in the cursor we return an empty list
        return []

    data = cursor.fetchall()
    result = []

    for row in data:
        resultrow = {}
        i = 0
        for dd in description:
            resultrow[dd[0]] = row[i]
            i += 1
        result.append(resultrow)
    return result

query = """SELECT d.blend, d.task, t.title, package, dependency, component FROM blends_dependencies d
             JOIN blends_tasks t ON d.task = t.task
             JOIN blends_metadata m ON d.blend = m.blend
             WHERE d.distribution = 'debian' AND m.distribution ilike 'Debian'"""
_execute_udd_query(query)
if curs.rowcount > 0:
    f = open('tasksdata.json', 'w')
    print >>f, json.dumps(RowDictionaries(curs))
    f.close()

query = "SELECT blend, outputdir FROM blends_metadata WHERE distribution ilike 'Debian'"
_execute_udd_query(query)
if curs.rowcount > 0:
    output=[]
    for row in RowDictionaries(curs):
	output.append(row)
    f = open('blendsurls.json', 'w')
    print >>f, json.dumps(output)
    f.close()
